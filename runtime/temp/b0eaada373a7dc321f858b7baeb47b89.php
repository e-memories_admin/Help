<?php /*a:1:{s:71:"D:\phpstudy_pro\help\application\home\view\voluntary\SearchNoClass.html";i:1625482749;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>志愿帮助系统</title>
    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-primary">
        <div class="panel-heading"></div>
        <div class="panel-body" id="form">
            <form action="" class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">学年</span>
                        <input class="form-control" type="text" v-model="year" placeholder="2020">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">学期</span>
                        <select class="form-control" v-model="term">
                            <option value="1">第一学期</option>
                            <option value="2">第二学期</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">周数</span>
                        <input class="form-control" type="number" v-model="week" placeholder="1">
                    </div>
                </div>
                <div class="form-group">
                    <div align="center">
                        <button type="button" v-on:click="search" class="btn btn-primary">查询</button>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
            </form>
        </div>
    </div>
</div>

    <div id="iframe" class="">
        <iframe :src="url" frameborder="0" width="100%" height="500%" scrolling="auto"></iframe>
    </div>

<script src="/static/home/js/vue.js"></script>
<script src="/static/home/js/vue-resource.js"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script>
    var form=new Vue({
        el:"#form",
        data:{
            year:'2020',
            term:'2',
            week:'1'
        },
        methods:{
            search:function () {
                iframe.url="SearchSchedule.html?year="+this.year+"&term="+this.term+"&week="+this.week;
            }
        }
    });
    var iframe=new Vue({
        el:"#iframe",
        data:{
            url:"",
        }
    })
</script>
</body>
</html>