<?php /*a:1:{s:65:"D:\phpstudy_pro\help\application/home/view\voluntary\MailBox.html";i:1625284254;}*/ ?>

<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="favicon.ico"> <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
  <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
  <link href="/static/home/css/plugins/iCheck/custom.css" rel="stylesheet">
  <link href="/static/home/css/animate.min.css" rel="stylesheet">
  <link href="/static/home/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-sm-9 animated fadeInRight">
      <div class="mail-box-header">

        <form method="get" class="pull-right mail-search">
          <div class="input-group">
            <div id="SearchContent">
              <input type="text" class="form-control input-sm" v-model="MailContent" placeholder="搜索邮件标题，正文等">
            </div>
            <div class="input-group-btn" id="SearchSubmit">
              <button type="button" v-on:click="search" class="btn btn-sm btn-primary">
                {{message}}
              </button>
            </div>
          </div>
        </form>
        <h2 id="num">
          收件箱 ({{num}})
        </h2>
        <div class="mail-tools tooltip-demo m-t-md">
          <div class="btn-group pull-right" id="page">
            <button class="btn btn-white btn-sm" v-on:click="minusPage" :disabled="startPage===currentPage">
              <i class="fa fa-arrow-left"></i>
            </button>
            <button class="btn btn-white btn-sm">
              {{currentPage}}
            </button>
            <button :class="'btn btn-white btn-sm'" v-on:click="addPage" :disabled="endPage===currentPage">
              <i class="fa fa-arrow-right"></i>
            </button>

          </div>
          <div class="row"></div>
        </div>
      </div>
      <div class="mail-box">
        <table class="table table-hover table-mail">
          <thead>
            <tr>
              <th class="check-mail">Id</th>
              <th class="mail-ontact">标题</th>
              <th class="mail-subject">内容</th>
              <th class="text-right">发件人</th>
              <th class="text-right">发件时间</th>
            </tr>
          </thead>
          <tbody>
          <tr v-for="row in rows"  :class="{'read':!row.read,'unread':row.read}" >
            <td class="check-mail">
              <a :href="'MailDetail?Id='+row.Id">
                {{row.Id}}
              </a>
            </td>
            <td class="mail-ontact">
              <a :href="'MailDetail?Id='+row.Id">
                {{row.title}}
              </a>
            </td>
            <td class="mail-subject">
              <a :href="'MailDetail?Id='+row.Id">
                {{row.content}}
              </a>
            </td>
            <td class="text-right">
              <a :href="'MailDetail?Id='+row.Id">
                {{row.fromUser}}
              </a>
            </td>
            <td class="text-right mail-date">
              <a :href="'MailDetail?Id='+row.Id">
                {{row.time}}
              </a>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- 全局js -->
<script src="/static/home/js/vue.js"></script>
<script src="/static/home/js/vue-resource.js"></script>
<script src="/static/home/js/MailBox.js"></script>
</body>
</html>
