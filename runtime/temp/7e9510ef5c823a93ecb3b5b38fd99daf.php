<?php /*a:1:{s:61:"D:\phpstudy_pro\help\application/index/view\index\forget.html";i:1621320449;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>志愿帮助平台 - 忘记密码</title>
    <meta name="keywords" content="志愿平台,志愿帮助平台,湖北工程学院,青年志愿者平台,青协,计算机学院,计算机与信息科学学院,计科,湖工青协,湖工">
    <meta name="description" content="青年志愿者平台是由计算机学院学生制作，面向于青协及全校学生的信息平台">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.1.0" rel="stylesheet">
    <title>志愿帮助平台-登录</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">HBU</h1>
        </div>
        <h3></h3>
        <form class="m-t" role="form" action="" method="post">
            <div class="form-group">
                <input type="text" id="qq" name="qq" class="form-control" placeholder="QQ" required="">
            </div>
            <button type="button" id="send" class="btn btn-primary block full-width m-b">发送验证邮件</button>
        </form>
    </div>
</div>
<div class="middle-box">
    <p class="text-center">Copyright &copy; 计科王宇哲</p>
</div>
<!-- 全局js -->
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script>
    $('#send').on('click',function (){
        var qq=$('#qq').val();
        $.ajax({
            url:"ForgetJson",
            type:"post",
            dataType:"json",
            data:{
                qq:qq
            },
            success:function (data){
                if(data.result==="success"){
                    layer.msg("邮件发送成功");
                    setTimeout(function (){
                        window.location.href="../../";
                    },2000);
                }else{
                    layer.msg(data.msg);
                }

            },
            error:function (){
                layer.msg("程序错误，请联系计科青协修复");
            }
        })
    })
</script>
</body>

</html>
