<?php /*a:1:{s:62:"D:\phpstudy_pro\help\application\home\view\voluntary\Memo.html";i:1627191908;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>志愿帮助系统</title>
    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/static/home/dist/summernote.css" rel="stylesheet"/>
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-primary">
        <div class="panel-heading">
            发送通知
        </div>
        <div class="panel-body">
            <form action="" class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-at">发送给</i></span>
                        <select class="form-control m-b" name="toMail" id="toMail" data-placeholder="请选择通知对象" multiple>

                        </select>
                    </div>

                </div>


                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-code">标题</i></span>
                        <input type="text" id="title" class="form-control" name="title" value="">
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment">内容</i></span>
<!--                        <textarea type="text" class="form-control" name="content" id="content" value=""></textarea>-->
                        <div class="summernote" id="content"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div align="center">
                        <button type="button" id="send" class="btn btn-primary">发送</button>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
            </form>
        </div>
    </div>
</div>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script src="/static/home/js/plugins/chosen/chosen.jquery.js"></script>
<script src="/static/home/dist/summernote.js"></script>
<script src="/static/home/dist/lang/summernote-zh-CN.js"></script>
<script>
    var content=$("#content");
    function sendFile(files, editor, editable) {
        var data1 = new FormData();
        data1.append('file', files);
        $.ajax({
            data: data1,
            type: 'POST',
            url: 'UploadImage',
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                content.summernote('insertImage', data.dir, 'img');
            },
            error: function () {
                layer.msg('文件上传失败');
            }
        })
    }
    content.summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold','italic', 'underline', 'clear','strikethrough','superscript','subscript']],
            ['fontsize',['fontname','fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontNames: ['微软雅黑','华文细黑','华文黑体','黑体','宋体','新宋体','仿宋','楷体','华文宋体','华文楷体','Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','Helvetica','Impact','Tahoma','Times New Roman','Verdana'],
        lang:'zh-CN',
        placeholder:"请在此输入通知内容",
        height:175,
        callbacks:{
            onImageUpload:function (files,editor,editable)
            {
                sendFile(files[0],editor,editable);
            }
        }
    })

    $("#send").on('click',function (){
        if(content.summernote('isEmpty'))
        {
            layer.msg("发件内容不能为空");
        }
        $.ajax({
            url:"SendMemo",
            type:"POST",
            dataType:"JSON",
            data:{
                toMail:$("#toMail").val(),
                title:$("#title").val(),
                content:$("#content").summernote('code'),
            },
            success:function (data){
                layer.msg(data.msg);
                $("#send").attr('disabled',true);
            },
            error:function (){
                layer.msg("发送失败");
            }
        });
    });
    var html='';
    html+="<option value='全体-全体'>全体同学</option>";
    html+="<option value='全体-青协'>全体青协</option>"
    $.ajax({
        url: "toMailJson",
        async:false,
        dataType: "Json",
        success:function (data) {
            $.each(data,function (index,datum) {
                html=html+"<optgroup label='"+datum.type+"'>";
                $.each(datum.data,function (index2,datum2) {
                    html=html+"<option value='"+datum2.username+"'>"+datum2.name+"</option>";
                })
                html=html+"</optgroup>"
            })
        },
        error:function (){
            layer.msg("信息加载失败");
        }
    });

    var toMail=$("#toMail");
    toMail.html(html);
    toMail.chosen({
         no_results_text: "无此结果",
         search_contains:true,
         allow_single_deselect:true,
         disable_search:false,
    });
</script>
</body>