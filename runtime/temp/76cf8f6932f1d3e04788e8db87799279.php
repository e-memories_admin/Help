<?php /*a:1:{s:63:"D:\phpstudy_pro\help\application\home\view\home\EditPerson.html";i:1621778503;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>志愿帮助系统</title>
    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-primary">
        <div class="panel-heading">
            修改资料
        </div>
        <div class="panel-body">
            <form action="" class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user">用户名</i></span>
                        <input type="text" id="name" class="form-control" value="" name="name">
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-venus-mars">性别</i></span>
                        <select class="form-control m-b" name="sex" id="sex">
                            <option value="1">男</option>
                            <option value="2">女</option>
                            <option value="3">无可奉告</option>
                        </select>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone">手机号</i></span>
                        <input type="tel" id="phone" class="form-control" value="" name="phone">
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-flask">专业</i></span>
                        <input type="text" id="major" class="form-control" name="major" value="">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-building">学院</i></span>
                        <input type="text" class="form-control" name="college" id="college" value="">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div align="center">
                        <button type="button" id="edit" class="btn btn-primary">确定修改</button>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
            </form>
        </div>
    </div>
</div>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/jquery.cookie.js"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script src="/static/home/js/plugins/suggest/bootstrap-suggest.min.js"></script>
<script>
    $.ajax({
        url:"UserDataJson",
        dataType:"JSON",
        success:function (data){
            $("#name").val(data.name);
            $("#sex").val(data.sex);
            $("#phone").val(data.phone);
            $("#major").val(data.major);
            $("#college").val(data.college);
        },
        error:function (){
            layer.msg("部门获取失败");
        }
    });
    $("#edit").on('click',function (){
        $.ajax({
            url:"EditJson",
            type:"POST",
            dataType:"JSON",
            data:{
                name:$("#name").val(),
                sex:$("#sex").val(),
                phone:$("#phone").val(),
                major:$("#major").val(),
                college:$("#college").val()
            },
            success:function (data){
                if(data.result==="success")
                {
                    layer.msg("修改成功");
                }else{
                    layer.msg(data.msg);
                }
            },
            error:function (){
                layer.msg("修改失败");
            }
        });
    });
    $("#major").bsSuggest({
        url:"MajorJson",
        effectiveFields: ["Id","major_name"],
        effectiveFieldsAlias: {"Id":"Id","major_name":"专业名称"},
        keyField: "major_name",
        idField:"Id",
        autoMinWidth: true,
        showBtn: true
    });
    $("#college").bsSuggest({
        url:"CollegeJson",
        effectiveFields: ["Id","college_name"],
        effectiveFieldsAlias: {"Id":"Id","college_name":"学院名称"},
        keyField: "college_name",
        idField:"Id",
        autoMinWidth: true,
        showBtn: true
    });
</script>
</body>