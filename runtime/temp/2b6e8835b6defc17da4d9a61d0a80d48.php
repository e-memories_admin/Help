<?php /*a:1:{s:59:"D:\phpstudy_pro\help\application/home/view\index\index.html";i:1625294475;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>志愿帮助系统</title>
    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
    <style>
        iframe{
            /*修复邮箱界面被覆盖*/
            position: relative;
            z-index: 999;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close"><i class="fa fa-times-circle"></i></div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span><img alt="image" class="img-circle" src="/static/home/icon.jpg" width="70" height="70"/></span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">
                                        <text class="UserName"></text>
                                        <br>
                                        <small class="UserAuth"></small>
                                    </strong>
                                </span>
                                <span class="text-muted text-xs block"><b class="caret"></b></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="J_menuItem" href="EditPerson">编辑个人资料</a>
                            </li>
                            <li class="divider"></li>
                            <li><a onclick="LoginOut()">安全退出</a>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">计科青协</div>
                </li>
                <li class="volun">
                    <a href="#">
                        <i class="fa fa fa-group"></i>
                        <span class="nav-label">青协系统</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem operator" href="../Volun/UserAdmir.html">用户管理</a>
<!--                            操作人员可见-->
                            <a class="J_menuItem" href="../Volun/Ecxellent.html">优秀志愿者</a>
                            <a class="J_menuItem" href="../Volun/Memo.html">发送内部通知</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="../Bbs/index" class="J_menuItem">
                        <i class="fa fa-comment"></i>
                        <span class="nav-label">留言板</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
<!--                        <div class="form-group">-->
<!--                            <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">-->
<!--                        </div>-->
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning" id="num"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages" id="List">
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>

        <div class="row content-tabs">
            <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i></button>
            <nav class="page-tabs J_menuTabs">

                <div class="page-tabs-content">
                    <a href="home.html" class="active J_menuTab" data-id="friend">首页</a>
                </div>
            </nav>
            <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
            </button>
            <div class="btn-group roll-nav roll-right">
                <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                    <li class="J_tabShowActive"><a>定位当前选项卡</a></li>
                    <li class="divider"></li>
                    <li class="J_tabCloseAll"><a>关闭全部选项卡</a></li>
                    <li class="J_tabCloseOther"><a>关闭其他选项卡</a></li>
                </ul>
            </div>
            <a href="loginout.html" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="home.html" frameborder="0" seamless data-id="home"></iframe>
        </div>
    </div>
    <!--右侧部分结束-->
    <div class="footer">
        <div class="pull-right">&copy; 2021<a href="#" target="_blank">计科王宇哲</a></div>
    </div>
</div>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/jquery.cookie.js"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script src="/static/home/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/home/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/home/js/hplus.min.js?v=4.0.0"></script>
<script type="text/javascript" src="/static/home/js/contabs.min.js"></script>
<script src="/static/home/js/plugins/pace/pace.min.js"></script>
<script>
    function LoginOut() {
        $.removeCookie('SafeCode',{ path: '/'}); //path为指定路径，直接删除该路径下的cookie
        layer.msg("退出成功!");
        setTimeout(function (){
            location.href="../../";
        },2000);
    }
    var UserData;
    $.ajax({    //用户信息
        async:false,
        url:'UserDataJson',
        type:"post",
        dataType:"json",
        success:function (data){
            if(data==null){
                layer.msg("请先登录!");
                setTimeout(function (){
                    location.href="../../";
                },1000);
            }else{
                UserData=data;
            }

        },
        error:function (){
            layer.msg("信息获取失败");
        }
    });

    $.ajax({
        url:"MsgJson",
        async:false,
        dataType: "JSON",
        success:function (data) {
            $("#num").html(data.num);
            htmlContent="";
            if(data.num!=0)
            {
                $.each(data.data,function (index,datum) {
                    htmlContent+="<li>\n" +
                        "                                <div class=\"dropdown-messages-box\">\n" +
                        "                                    <a href=\"../Volun/MailDetail?Id="+datum.Id+"\" class=\"pull-left J_menuItem\">\n" +
                        "                                        <img alt=\"image\" class=\"img-circle\" src=\"http://q1.qlogo.cn/g?b=qq&nk="+datum.qq+"&s=640\">\n" +
                        "                                    </a>\n" +
                        "                                    <div class=\"media-body \">\n" +
                        "                                        <strong>"+datum.fromUser+"</strong> <p>"+datum.content+"</p>  " +
                        "                                        <br>\n" +
                        "                                        <small class=\"text-navy\">"+datum.time+"</small>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                            </li>\n" +
                        "                            <li class=\"divider\"></li>"
                });
            }else{
                htmlContent+="<li>\n" +
                    "                                <div class=\"text-center link-block\">\n" +
                    "                                       <strong>暂无消息...</strong>\n" +
                    "                                </div>\n" +
                    "                            </li>\n"+
                    "                            <li class=\"divider\"></li>";
            }
            htmlContent+="<li>\n" +
                "                                <div class=\"text-center link-block\">\n" +
                "                                    <a class=\"J_menuItem\" href=\"../volun/MailBox.html\">\n" +
                "                                        <i class=\"fa fa-envelope\"></i> <strong> 查看所有消息</strong>\n" +
                "                                    </a>\n" +
                "                                </div>\n" +
                "                            </li>";
            $("#List").html(htmlContent);
        },
        error:function () {
            layer.msg("通知信息获取失败")
        }
    })

    if(UserData.operator===false){
        $('.operator').hide();
    }
    if (UserData.auth==="学生")   //TODO 若修改普通用户记得改这边
    {
        $(".volun").hide();
    }
    $(".UserAuth").text(UserData.auth);
    $(".UserName").text(UserData.name);
</script>
</body>
