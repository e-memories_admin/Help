<?php /*a:1:{s:58:"D:\phpstudy_pro\help\application/home/view\bbs\AddBbs.html";i:1621775810;}*/ ?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="/static/home/css/bootstrap.min.css">
  <link href="/static/home/dist/summernote.css" rel="stylesheet"/>
  <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
  <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
  <link href="/static/home/css/animate.min.css" rel="stylesheet">
</head>
<body>
<div class="summernote" id="summernote"></div>
<div class="pull-right">
    <button type="button" id="post" class="btn btn-primary">
    <i class="fa fa-check">发表</i>
    </button>
</div>

</body>
<script src="/static/home/js/jquery.min.js"></script>
<script src="/static/home/js/bootstrap.min.js"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script src="/static/home/dist/summernote.js"></script>
<script src="/static/home/dist/lang/summernote-zh-CN.js"></script>    <!-- 中文-->
<script>
    $('#post').on('click', function () {
        if ($('#summernote').summernote('isEmpty')) {
            layer.msg('内容不能为空');
        } else {
            var content= $('#summernote').summernote('code');
            $.ajax({
                data: {
                    content: content
                },
                type: 'POST',
                url: 'AddBbsJson',
                dataType: 'JSON',
                success: function (data) {
                    if (data.result === 'success') {
                        layer.msg('发表成功');
                        window.close();
                        $('#summernote').summernote('code', '');
                    } else {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        layer.msg('发表失败');
                    }
                },
                error: function () {
                    layer.msg('发表失败');
                }
            })
        }
    })
    $(document).ready(function () {

        $('#summernote').summernote({
            lang: 'zh-CN',
            placeholder: '请在此输入你的留言...',
            height: 200,
            callbacks: {
                onImageUpload: function (files, editor, editable) {
                    sendFile(files[0], editor, editable);
                }
            }
        });
    });

    function sendFile(files, editor, editable) {
        var data1 = new FormData();
        data1.append('file', files);
        $.ajax({
            data: data1,
            type: 'POST',
            url: '../Volun/UploadImage.html',
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                $('#summernote').summernote('insertImage', data.dir, 'img');
            },
            error: function () {
                layer.msg('文件上传失败');
            }
        })
    }
</script>
</html>
