<?php /*a:1:{s:72:"D:\phpstudy_pro\help\application\home\view\voluntary\searchSchedule.html";i:1625711445;}*/ ?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico"> <link href="/static/home/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-primary">
        <div class="panel-heading">
            查询无课表
        </div>
        <div class="panel-body" id="form">
            <form action="" class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">学年</span>
                        <input class="form-control" type="text" v-model="year" placeholder="2020">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">学期</span>
                        <select class="form-control" v-model="term">
                            <option value="1">第一学期</option>
                            <option value="2">第二学期</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">周数</span>
                        <input class="form-control" type="number" v-model="week" placeholder="1">
                    </div>
                </div>
                <div class="form-group">
                    <div align="center">
                        <button type="button" v-on:click="search" class="btn btn-primary">查询</button>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
            </form>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins" id="table">
                <div class="ibox-title">
                    <h5>无课表第{{week}}周</h5>
                </div>
                <div class="ibox-content">
                    <p>第{{week}}周无课表已经完成，请查看</p>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>节次</th>
                            <th>周一</th>
                            <th>周二</th>
                            <th>周三</th>
                            <th>周四</th>
                            <th>周五</th>
                            <th>周六</th>
                            <th>周日</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="i in 12">
                            <td>第{{i}}节</td>
                            <td v-for="j in 7" v-on:click="print(i,j)">
                               查看
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="/static/home/js/vue.js"></script>
<script src="/static/home/js/vue-resource.js"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script>
    var w=['一','二','三','四','五','六','日']
    var table=new Vue({
        el:"#table",
        data:{
            week:'1',
            year:'2020',
            term:'1'
        },
        methods:{
            print:function (a,b)
            {
                layer.open({
                    title:"周"+w[b-1]+"第"+a+"节课无课人员",
                    type:2,
                    content:"WithoutClass.html?week="+this.week+"&class="+a+"&weekDay="+b+"&year="+this.year+"&term="+this.term,
                    area: ['500px','540px'],
                })
            }
        }
    })
    var form=new Vue({
        el:"#form",
        data:{
            week:'1',
            year:'2020',
            term:'1',
        },
        methods:{
            search:function () {
                table.week=this.week;
                table.year=this.year;
                table.term=this.term;
            }
        }
    });
</script>
</body>

</html>
