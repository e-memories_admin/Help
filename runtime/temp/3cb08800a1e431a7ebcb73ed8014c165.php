<?php /*a:1:{s:57:"D:\phpstudy_pro\help\application\home\view\bbs\index.html";i:1626066874;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->

    <link href="/static/home/css/datepicker3.css" rel="stylesheet">
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
</head>
<body>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>留言板</h2>
        <ol class="breadcrumb">
            <li href="index">
                留言板
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-sm-8">
            <!--发表留言-->
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>留言发表</h5>
                </div>
                <div class="ibox-content">
                    <div class="text-center well">
                        <a data-toggle="modal" class="btn btn-primary" id="add">发表留言</a>
                    </div>
                </div>
            </div>
            <!--最新动态-->
            <div class="ibox float-e-margins">
                <!--容器标题-->
                <div class="ibox-title">
                    <h5>最新留言</h5>
                </div>
                <div class="social-feed-box" id="BbsContent"></div>
                <div class="text-center">
                    <ul class="pagination" id="Page">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="/static/home/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/home/js/bootstrap.min.js?v=3.3.5"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script src="/static/home/js/Bbs.js"></script>
</html>