<?php /*a:1:{s:70:"D:\phpstudy_pro\help\application\home\view\voluntary\copySchedule.html";i:1625458142;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>志愿帮助系统</title>
    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/home/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/home/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/home/css/animate.min.css" rel="stylesheet">
    <link href="/static/home/css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="/static/home/js/layui/css/layui.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-danger">
        <div class="panel-heading">帮助信息</div>
        <div class="panel-body">
            <ol class="">
                <li>1、下载超级课程表</li>
                <li>2、注册并登录超级课程表之后，导入课程表</li>
                <li>3、在课程表页面右上角点击分享，得到一个分享二维码</li>
                <li>4、手机扫码进入网页，将最终进入的网页网址粘贴到下列内容框中，点击导入</li>
            </ol>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="panel panel-primary">
        <div class="panel-heading">导入课程表</div>
        <div class="panel-body" id="form">
            <form action="" class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment">内容</i></span>
                        <textarea v-model="Content" class="form-control" cols="20" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div align="center">
                        <button type="button" v-on:click="Export" class="btn btn-primary">导入</button>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>
            </form>
        </div>
    </div>
</div>
<script src="/static/home/js/vue.js"></script>
<script src="/static/home/js/vue-resource.js"></script>
<script src="/static/home/js/layui/layui.all.js"></script>
<script>
    function getQueryVariable(url,variable)
    {
        var query = url;
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] === variable){return pair[1];}
        }
        return false;
    }
    var form=new Vue({
        el:"#form",
        data:{
            'Content':'',
            'superId':''
        },
        methods:{
            Export:function () {
                this.superId=getQueryVariable(this.Content,'i');
                this.$http.post('updateSuperId',{superId:this.superId}).then(
                    (data)=>{
                        if(data.body.result==='success')
                            layer.msg('导入信息完成！');
                        else
                            layer.msg('导入失败');
                    },
                    (err)=>{
                        layer.msg('导入失败！');
                    }
                )


            }
        }
    })
</script>
</body>
</html>