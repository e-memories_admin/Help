var $add=$('#add'),
    $delete=$('#delete'),
    $table=$('#tablelist');
$add.prop('disabled',false);
$delete.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {
        function delete_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'delete_arrange_table.html',
                data:ids,
                dataType:'JSON',
                success:function (){
                    layer.msg('删除成功');
                    $table.bootstrapTable('refresh');
                },
                error:function (){
                    layer.msg('删除失败');
                }
            })
        }
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $add.on('click',function (){
            //if (isInArray([4,3],authority)){
            layer.open({
                type: 2,
                title: '增加新项目',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '60%'],
                content: 'insert_arrange_table.html?Id='+$Id,
                end:function (){
                    $table.bootstrapTable('refresh');
                }
            });
            //}else{
            //   layer.msg('你无权操作');
            //}

        })
        $delete.on('click',function (){
                var ids=getSelections();
                if (ids.length==0){
                    layer.msg('请先选择至少一个项目');
                }else{
                    layer.confirm('确定删除这'+ids.length+'个项目？',{
                        btn:['是','否']
                    },function (){
                        delete_all(ids);
                    })
                }

        })
        $table.bootstrapTable({
            url: "arrange_table_json.html?Id="+$Id,
            dataType:'json',
            method:'get',
            cardView:true,
            search: true,
            pagination: true,
            showRefresh: true,
            //showToggle: true,
            showColumns: true,
            clickToSelect: true,
            //showExport: true,
            //exportDataType: "base",
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                 toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt'
            },
            columns: [{
                field: 'state',
                checkbox:true
            },{
                title:"实验项目名称",
                field:"item_name",
                editable:{
                    type:"text"
                }
            },{
                title:"学时",
                field:"hour",
                editable: {
                    type: "number"
                }
            },{
                title:"相同实验组数",
                field:"similar_num",
                editable:{
                    type:"number"
                }
            },{
                title: "每组人数",
                field:"each_num",
                editable:{
                    type:"number",
                }
            },{
                title: "实验批数",
                field:"num",
                editable:{
                    type:"number"
                }
            },{
                title: "开课时间",
                field:"arrange_time",
                editable:{
                    type:"text",
                }
            },{
                title: "备注",
                field:"comment",
                editable:{
                    type:"textarea"
                }
            }],
            onEditableSave:function (field, row, oldValue, $el) {
                row['field']=field;
                $.ajax({
                    type: "post",
                    url: "edit_arrange_table.html",
                    data: row,
                    dataType: 'JSON',
                    success: function (data, status) {
                        if (status == "success") {
                            layer.msg("编辑成功");
                        }
                    },
                    error: function () {
                        layer.msg("编辑失败");
                    },
                    complete: function () {

                    }

                });
            }
            // //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            // //basic', 'all', 'selected'.
            // exportTypes:['json','xml','csv','txt','sql','doc','excel'],	    //导出类型
            // //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            // exportOptions:{
            //     //ignoreColumn: [0,0],            //忽略某一列的索引
            //     fileName: '数据导出',              //文件名称设置
            //     worksheetName: 'Sheet1',          //表格工作区名称
            //     tableName: '商品数据表',
            //     //onMsoNumberFormat: 'DoOnMsoNumberFormat'
            // }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool2=!(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled',bool2)
        })
    })();
})(document, window, jQuery);
