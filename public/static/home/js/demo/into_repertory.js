
/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

function cellStyle(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function rowStyle(row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (index % 2 === 0 && index / 2 < classes.length) {
        return {
            classes: classes[index / 2]
        };
    }
    return {};
}

function scoreSorter(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}

function nameFormatter(value) {
    return value + '<i class="icon wb-book" aria-hidden="true"></i> ';
}

function starsFormatter(value) {
    return '<i class="icon wb-star" aria-hidden="true"></i> ' + value;
}

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 100,
        page: 1
    };
}

function buildTable($el, cells, rows) {
    var i, j, row,
        columns = [],
        data = [];

    for (i = 0; i < cells; i++) {
        columns.push({
            field: '字段' + i,
            title: '单元' + i
        });
    }
    for (i = 0; i < rows; i++) {
        row = {};
        for (j = 0; j < cells; j++) {
            row['字段' + j] = 'Row-' + i + '-' + j;
        }
        data.push(row);
    }
    $el.bootstrapTable('destroy').bootstrapTable({
        columns: columns,
        data: data,
        iconSize: 'outline',
        icons: {
            columns: 'glyphicon-list'
        }
    });
}

function ajaxRequest(params){
    $.ajax({
        url: 'demo.php',
        type: 'POST',
        dataType: 'json',
        success:function (rs){
            console.log(rs);
            var message=rs.array;
            params.success({
                total:rs.total,
                rows:message
            })
        },
        error:function (rs){
            console.log(rs)
        }
    })
}
function isInArray(arr,value){
    for(var i = 0; i < arr.length; i++){
        if(value === arr[i]){
            return true;
        }
    }
    return false;
}
var $edit=$('#edit'),
    $table=$('#purchaselist');

$edit.prop('disabled',true);
(function(document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function() {

        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $edit.on('click',function (){
            var row=getSelections()[0];
            var Id=row.Id;
            layer.open({
                type: 2,
                title: '耗材入库',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '60%'],
                content: 'into.html?Id=' + Id,
                end:function (){
                    $table.bootstrapTable('refresh');
                }
            });

        })
        $table.bootstrapTable({
            url: "purchase_json.html",
            dataType:'json',
            method:'post',
            search: true,
            clickToSelect: true,
            pagination: true,
            showRefresh: true,
            showToggle: true,
            showColumns: true,
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            },
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool=!(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length==1
            )
            $edit.prop('disabled',bool);
        })
    })();
})(document, window, jQuery);
