var $delete=$('#delete'),
    $table=$('#UserList');
$delete.prop('disabled',true);
var CollegeList=[];
var DepartList=[];
var AuthList=[
    {value:"1",text:"校青协主席"},
    {value:"2",text:"会长"},
    {value:"3",text:"网站管理员"},
    {value:"4",text:"部长"},
    {value:"5",text:"干事"},
    {value:"0",text:"普通学生"}
];



$.ajax({
    url:"../Home/DepartJson",
    async:false,
    dataType:"JSON",
    success:function (data){
        $.each(data,function (index,datum) {
            DepartList.push({value:datum.depart,text:datum.depart});
        })
    },
    error:function (){
        layer.msg("部门获取失败");
    }
});



$.ajax({
    url:"../Home/CollegeJson",
    async:false,
    dataType:"JSON",
    success:function (data){
        $.each(data.value,function (index,datum) {
            CollegeList.push({value:datum.college_name,text:datum.college_name});
        })
    },
    error:function (){
        layer.msg("部门获取失败");
    }
});




(function(document, window, $) {
    // Example Bootstrap Table Events
    // ------------------------------
    (function() {

        function delete_all(ids){
            ids=JSON.stringify(ids);
            $.ajax({
                type:'POST',
                url:'DeleteUser.html',
                data:ids,
                dataType:'JSON',
                success:function (data){
                    if(data.result==="success"){
                        layer.msg('删除成功');
                        $table.bootstrapTable('refresh');
                    }
                },
                error:function (){
                    layer.msg('删除失败');
                }
            })
        }
        function getSelections(){
            return $.map($table.bootstrapTable('getSelections'),function (row){
                return row;
            })
        }
        $delete.on('click',function (){
                var ids=getSelections();
                if (ids.length==0){
                    layer.msg('请先选择至少一个用户');
                }else{
                    layer.confirm('确定删除这'+ids.length+'个用户？',{
                        btn:['是','否']
                    },function (){
                        delete_all(ids);
                    })
                }
        })
        $table.bootstrapTable({
            url: "UserJson.html",
            dataType:'json',
            method:'post',
            search: true,
            pagination: true,
            showRefresh: true,
            // showToggle: true,
            showColumns: true,
            clickToSelect: true,
            showExport: true,
            exportDataType: "base",
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list-alt'
            },
            columns: [{
                field:'state',
                checkbox:true
            },{
                field:'Id',
                title:'Id'
            },{
                field:'username',
                title:'账号'
            },{
                field:'name',
                title:'用户名',
                editable: {
                    type: "text"
                }
            },{
                field:'sex',
                title:'性别',
                editable: {
                    type: "select",
                    source: [
                        {value:"1",text:"男"},
                        {value:"2",text:"女"},
                        {value:"3",text:"无可奉告"}
                    ]
                }
            },{
                field:'phone',
                title:'手机号',
                editable: {
                    type: "tel"
                }
            },{
                field:'major',
                title:'专业',
                editable: {
                    type: "text"
                }
            },{
                field:'college',
                title:'学院',
                editable: {
                    // type: "text"
                    type: "select",
                    source: CollegeList
                }
            },{
                field:'department',
                title:'部门',
                editable: {
                    type: "select",
                    source: DepartList
                }
            },{
                field:'auth',
                title:'职位',
                editable: {
                    type: "select",
                    source: AuthList
                }
            }],
            onEditableSave:function (field,row,oldValue,$el){
                row["field"]=field;
                $.ajax({
                    type:"post",
                    url:"EditUser.html",
                    data:row,
                    dataType:"JSON",
                    success:function (data){
                        if(data.result==="success"){
                            layer.msg("编辑成功");
                        }else{
                            layer.msg(data.msg);
                        }
                    },
                    error:function (){
                        layer.msg("编辑失败");
                    },
                    complete: function () {

                    }
                })
            }
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
            var bool2=!(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled',bool2)
        })
    })();
})(document, window, jQuery);
