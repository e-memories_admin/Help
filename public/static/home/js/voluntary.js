var voluntary=new Vue({
    el:"#voluntary",
    data:{
        //页面显示及分页
        VolunList:[],   //全部数据列表
        currentData:[],    //当前数据列表
        each:9, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数

        //搜索
        searchContent:""   //搜索内容
    },
    methods:{
        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.VolunList.length);i++)
                {
                    this.currentData.push(this.VolunList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.VolunList.length);i++)
                {
                    this.currentData.push(this.VolunList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.VolunList.length);i++)
            {
                this.currentData.push(this.VolunList[i]);
            }
        },

        //搜索操作
        search:function () {
            this.$http.post('VolunJson.html',{content:this.searchContent}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.VolunList=[];
                    this.currentData=[];


                    this.VolunList=data.body;
                    var len=this.VolunList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.VolunList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                },
                (err)=>
                {
                    console.log(err);
                }
            )
        }
    },
    created:function () {
        this.$http.get('VolunJson.html').then(
            (data)=>
            {
                this.pages=[];

                this.VolunList=data.body;


                var len=this.VolunList.length;
                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.VolunList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            },
            (err)=>
            {
                console.log(err);
            }
        )
    }
});