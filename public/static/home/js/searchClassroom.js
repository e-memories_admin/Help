/**
 * 查询楼号
 */

function updateTable() {

    let year = $year.val();
    let term = $term.val();
    let lh = $lh.val();
    let week = selectRange.value;
    let day = selectRange1.value;
    let time = selectRange2.value;
    let url = "searchJson.html?xnm=" + year + "&xqm=" + term + "&lh="+lh+'&week='+week+'&day='+day+'&times='+time;
    let opt = {
        url: url
    };
    $table.bootstrapTable('refresh', opt);
}

var form=new Vue({
    el:"#form",
    data:{
        buildingList:[],
    },
    mounted(){
        this.init();
    },
    methods:{
        init(){
            this.$http.get('BuildingJson').then(
                (data)=>
                {
                    form.buildingList=data.body;
                },
                (err)=>{
                    layer.msg("加载失败"+err);
                }
            )
        },
    }
});


/**
 * 多选插件-周
 */

var selectRange=new Vue({
    el:"#selectRange",
    data:{
        value:0,
        isSelect:[
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        ]
    },

    methods: {
        select:function (i){


            if (this.isSelect[i]){
                this.value-=(1<<(i-1));
            }else{
                this.value+=(1<<(i-1));
            }

            //这样更改数组值才能引起试图更新
            Vue.set(this.isSelect,i,!this.isSelect[i]);

            updateTable();
        }
    }
});

/**
 * 多选插件-星期
 */

var selectRange1=new Vue({
    el:"#selectRange1",
    data:{
        value:0,
        isSelect:[
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        ]
    },

    methods: {
        select:function (i){


            if (this.isSelect[i]){
                this.value-=(1<<(i-1));
            }else{
                this.value+=(1<<(i-1));
            }

            //这样更改数组值才能引起试图更新
            Vue.set(this.isSelect,i,!this.isSelect[i]);


            updateTable();
        }
    }
});

/**
 * 多选插件-节次
 */

var selectRange2=new Vue({
    el:"#selectRange2",
    data:{
        value:0,
        isSelect:[
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        ]
    },

    methods: {
        select:function (i){


            if (this.isSelect[i]){
                this.value-=(1<<(i-1));
            }else{
                this.value+=(1<<(i-1));
            }

            //这样更改数组值才能引起试图更新
            Vue.set(this.isSelect,i,!this.isSelect[i]);

            updateTable();
        }
    }
});


$(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        autoclose: true,//选中之后自动隐藏日期选择框
        clearBtn: true,//清除按钮
        viewMode: "years",
        minViewMode: "years",
        format: "yyyy"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
});

var $year = $('#year'),
    $term = $('#term'),
    $lh = $('#lh');


$year.on('change', function () {
    updateTable();
});
$term.on('change', function () {
    updateTable();
});

$lh.on('change', function () {
    updateTable();
});


var $table = $('#RoomList');
(function (document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function () {
        $table.bootstrapTable({
            url: "searchJson",
            dataType: 'json',
            method: 'post',
            search: true,
            pagination: true,
            clickToSelect: true,
            showRefresh: true,
            showToggle: true,
            pageSize:7,
            showColumns: true,
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            },
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            var bool = !(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length == 1
            )
            $edit.prop('disabled', bool);
            $look.prop('disabled', bool);
            var bool2 = !(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled', bool2)
        })
    })();
})(document, window, jQuery);
