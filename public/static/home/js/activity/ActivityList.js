function step(value){
    if(value=="活动完成") return '<button class="btn btn-primary btn-xs">' + value + '</button>';
    return '<button class="btn btn-danger btn-xs">' + value + '</button>';
}

/**
 * 跳转到管理页面
 * @param Id 管理的活动Id
 */
function manage(Id){
    window.location.href="manageActivity?Id=" + Id;
}

function op(value){
    return '<button class="btn btn-info btn-xs" onclick="manage(' + value + ')">管理</button>';
}


var $delete=$('#delete'),
    $add=$("#addNew"),
    $table=$('#ActivityList');
$delete.prop('disabled',true);

function delete_all(ids){
    ids=JSON.stringify(ids);
    $.ajax({
        type:'POST',
        url:'DeleteActivity.html',
        data:ids,
        dataType:'JSON',
        success:function (data){
            if(data.result==="success"){
                layer.msg('删除成功');
                $table.bootstrapTable('refresh');
            }
        },
        error:function (){
            layer.msg('删除失败');
        }
    })
}
function getSelections(){
    return $.map($table.bootstrapTable('getSelections'),function (row){
        return row;
    })
}

$add.on('click',function (){
    layer.open({
        type: 2,
        title: '新增活动',
        shadeClose: false,
        shade: 0.8,
        area: ['50%', '60%'],
        content: 'addNewActivity.html',
        end:function (){
            $table.bootstrapTable('refresh');
        }
    });
})
$delete.on('click',function (){
    var ids=getSelections();
    if (ids.length==0){
        layer.msg('请先选择至少一个活动');
    }else{
        layer.confirm('确定删除这'+ids.length+'个活动？',{
            btn:['是','否']
        },function (){
            delete_all(ids);
        })
    }
})
$table.bootstrapTable({
    url: "ActivityListJson.html",
    dataType:'json',
    method:'post',
    search: true,
    pagination: true,
    showRefresh: true,
    // showToggle: true,
    showColumns: true,
    clickToSelect: true,
    showExport: true,
    exportDataType: "base",
    iconSize: 'outline',
    toolbar: '#exampleTableEventsToolbar',
    icons: {
        refresh: 'glyphicon-repeat',
        toggle: 'glyphicon-list-alt',
        columns: 'glyphicon-list-alt'
    },
    columns: [{
        field:'state',
        checkbox:true
    },{
        field:'name',
        title:'活动名称',
        editable: {
            type: "text"
        }
    },{
        field:'location',
        title:'活动地点',
        editable: {
            type: "text"
        }
    },{
        field:'activity_time',
        title:'活动时间',
        editable: {
            type: "text"
        }
    },{
        field:'comment',
        title:'活动描述',
        editable: {
            type: "textarea"
        }
    },{
        field:'creator',
        title:'创建人',
    },{
        field:'college_name',
        title:'归属学院',
    },{
        field:'participant_num',
        title:'参与人数',
    },{
        field:'step',
        title:'当前步骤',
        formatter:'step'
    },{
        field:'Id',
        title:'操作',
        formatter: "op"
    }],
    onEditableSave:function (field,row,oldValue,$el){
        row["field"]=field;
        $.ajax({
            type:"post",
            url:"EditActivityJson.html",
            data:row,
            dataType:"JSON",
            success:function (data){
                if(data.result==="success"){
                    layer.msg("编辑成功");
                }else{
                    layer.msg(data.msg);
                }
            },
            error:function (){
                layer.msg("编辑失败");
            },
            complete: function () {

            }
        })
    }
});
$table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',function (){
    var bool2=!(
        $table.bootstrapTable('getSelections').length
    )
    $delete.prop('disabled',bool2)
});
