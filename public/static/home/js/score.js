/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        autoclose: true,//选中之后自动隐藏日期选择框
        clearBtn: true,//清除按钮
        viewMode: "years",
        minViewMode: "years",
        format: "yyyy"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
});
$('#insert').on('click', function () {
    layer.open({
        type: 2,
        title: '对接教务系统',
        shadeClose: false,
        shade: 0.8,
        area: ['50%', '60%'],
        content: 'insert_sid.html',
        end: function () {
            $table.bootstrapTable('refresh');
        }
    });
})
var $year = $('#year'),
    $term = $('#term');


$year.on('change', function () {
    var year = $year.val();
    var term = $term.val();
    var url = "score_json.html?year=" + year + "&term=" + term;
    var opt = {
        url: url
    };
    $table.bootstrapTable('refresh', opt);
});
$term.on('change', function () {
    var year = $year.val();
    var term = $term.val();
    var url = "score_json.html?year=" + year + "&term=" + term;
    var opt = {
        url: url
    };
    $table.bootstrapTable('refresh', opt);
});
// 使用刚指定的配置项和数据显示图表。
var myChart = echarts.init(document.getElementById('main'));
$('#grape').on('click',function () {

    var year = $year.val();
    var term = $term.val();
    if (year===''||term===0)
    {
        year='2020';
        term='1';
    }
    $.get('score_json.html?year=' + year + '&term=' + term, function (data) {
        var scores = data.data;
        var s = [], c = [], o = [];
        for (k in scores) {
            s.push(scores[k].score);
            c.push(scores[k].courseName);
            o.push(scores[k].order);
        }
        console.log(c);
        console.log(s);
        console.log(o);
        myChart.setOption({

            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            legend: {
                data: ['科目', '排名', '成绩']
            },
            xAxis: {
                type: 'category',
                data: c   //TODO 科目列表
            },
            yAxis: [
                {
                    type: 'value',
                    name: '成绩',
                    min: 0,
                    max: 100,
                    interval: 20,

                    axisLabel: {
                        formatter: '{value} 分'
                    }
                },
                {
                    type: 'value',
                    name: '排名',
                    min: 1,
                    max: data.sum + 1,
                    interval: 1,
                    inverse: true,
                    axisLabel: {
                        formatter: '{value} 名'
                    }
                }
            ],
            toolbox: {
                show: true,
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    dataView: {
                        readOnly: true
                    },
                    magicType: {
                        type: ['line', 'bar']
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            series: [{
                name: '成绩',
                type: 'bar',
                showBackground: true,
                backgroundStyle: {
                    color: 'rgba(180, 180, 180, 0.2)'
                },
                markPoint: {
                    data: [
                        {type: 'max', name: '最高成绩'},
                        {type: 'min', name: '最低成绩'}
                    ]
                },
                markLine: {
                    data: [
                        {type: 'average', name: '平均分'}
                    ]
                },
                data: s   // 成绩数据列表
            },
                {
                    name: '排名',
                    type: 'line',
                    markPoint:
                        {
                            data: [
                                {type: 'max', name: '最高排名'},
                                {type: 'min', name: '最低排名'}
                            ]
                        },
                    markLine: {
                        data: [
                            {type: 'average', name: '平均排名'}
                        ]
                    },
                    yAxisIndex: 1,
                    data: o     // 排名列表
                }]
        });
    })
})

var timer = window.setInterval(function () {
    myChart.resize();
}, 1500);
window.addEventListener("resize", () => {
    myChart.resize();
});

var $table = $('#userlist');
(function (document, window, $) {


    // Example Bootstrap Table Events
    // ------------------------------
    (function () {
        $table.bootstrapTable({
            url: "score_json",
            dataType: 'json',
            method: 'post',
            search: true,
            pagination: true,
            clickToSelect: true,
            showRefresh: true,
            showToggle: true,
            pageSize:7,
            showColumns: true,
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            },
        });
        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            var bool = !(
                $table.bootstrapTable('getSelections').length &&
                $table.bootstrapTable('getSelections').length == 1
            )
            $edit.prop('disabled', bool);
            $look.prop('disabled', bool);
            var bool2 = !(
                $table.bootstrapTable('getSelections').length
            )
            $delete.prop('disabled', bool2)
        })
    })();
})(document, window, jQuery);
