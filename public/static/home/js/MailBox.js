var n=0;

var num=new  Vue({
    el:"#num",
    data:{
        num:0
    }
});

var table=new Vue({
    el:".mail-box",
    created:function () {
        this.$http.get("MailJson")
            .then((data)=>{
                var R=data.body;
                table.Rows=R;
                num.num=R.length;
                table.length=R.length;
                page.endPage=Math.ceil(table.length/table.eachPage);
                var r=[];
                page.maxNum=Math.min(table.length-1,table.eachPage-1);
                for (var n=0;n<Math.min(table.length,table.eachPage);n++)
                {
                    r.push(R[n]);
                }
                table.rows=r;
            },(err)=>{
                console.log(err);
            })
    },
    data:{
        length:0,
        rows:[],
        Rows:[],
        eachPage:10,
    },
    methods:{}
});

var SearchContent=new Vue({
    el:"#SearchContent",
    data:{
        MailContent:""
    }
});

var SearchSubmit=new Vue({
    el:"#SearchSubmit",
    data:{
        message:"搜索"
    },
    methods:{
        search:function (){
            var content=SearchContent.MailContent;
            this.$http.post("MailJson",{content:content}).then((data)=>{
                //重新初始化
                page.startPage=1;
                page.currentPage=1;
                page.endPage=1;
                page.minNum=0;
                page.maxNum=10;


                var R=data.body;
                table.Rows=R;
                num.num=R.length;
                table.length=R.length;
                page.endPage=Math.ceil(table.length/table.eachPage);
                var r=[];
                page.maxNum=Math.min(table.length-1,table.eachPage-1);
                for (var n=0;n<Math.min(table.length,table.eachPage);n++)
                {
                    r.push(R[n]);
                }
                table.rows=r;
            },(err)=>{
                console.log(err);
            })
        }
    }
});

var page=new Vue({
    el:"#page",
    data:{
        startPage:1,
        currentPage:1,
        endPage:1,
        minNum:0,
        maxNum:10
    },
    methods:{
        addPage:function () {
            if(page.currentPage<page.endPage)
            {
                page.currentPage++;
                page.minNum+=table.eachPage;
                page.maxNum+=table.eachPage;
            }
            var r=[];
            for (var n=page.minNum;n<Math.min(page.maxNum,table.length);n++)
            {
                r.push(table.Rows[n]);
            }
            table.rows=r;
        },
        minusPage:function () {
            if(page.currentPage>page.startPage)
            {
                page.currentPage--;
                page.minNum-=table.eachPage;
                page.maxNum-=table.eachPage;
            }
            var r=[];
            for (var n=page.minNum;n<Math.min(page.maxNum,table.length);n++)
            {
                r.push(table.Rows[n]);
            }
            table.rows=r;
        }
    }
});