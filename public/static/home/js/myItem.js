var thing=new Vue({
    el:"#thing",
    data:{
        radarList:[],
        currentData:[],    //当前数据列表
        each:6, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数


        tagName:[], //标签名称列表
        searchContent:'',
    },
    methods:{

        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
            {
                this.currentData.push(this.radarList[i]);
            }
        },

        search:function ()
        {
            this.$http.post('RadarListJson?type=thing',{content:this.searchContent}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                },
                (err)=>
                {
                    console.log(err);
                }
            );
        },

        addList:function ()     //寻找物品
        {
            layer.open({
                type: 2,
                title: '寻找丢失物品',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'addThing?type=thing',
                end: function () {
                    location.reload();
                }
            });
        },

        search2:function () {
            this.$http.post('SearchByTag?type=thing',{content:$('#select1').val()}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                }
            )
        },

        getByTag:function (tag) {
            this.$http.post('SearchByTag?type=thingByTagName',{content:tag}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                }
            )
        },

        getDetail:function (radar_id) {
            layer.open({
                type: 2,
                title: '物品详情',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'thingDetail?type=thing&radar_id='+radar_id,
            });
        },
    },
    created:function () {
        this.$http.get('RadarListJson?type=thing').then(
            (data)=>
            {
                this.pages=[];

                this.radarList=data.body;

                var len=this.radarList.length;
                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            }
        );

        this.$http.get('TagList?type=thing').then(
            (data)=>
            {
                this.tagName=data.body;

                setTimeout(function () {
                    $(".chosen-select").chosen({
                        no_results_text: "没有找到结果！",//搜索无结果时显示的提示
                        search_contains:true,   //关键字模糊搜索，设置为false，则只从开头开始匹配
                        allow_single_deselect:true, //是否允许取消选择
                    })
                },'800');
            }
        );

    }
});

var master=new Vue({
    el:"#master",
    data:{
        radarList:[],
        currentData:[],    //当前数据列表
        each:6, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数

        tagName:[],
        searchContent:''
    },
    methods:{

        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
            {
                this.currentData.push(this.radarList[i]);
            }
        },

        search:function () {
            this.$http.post('RadarListJson?type=master',{content:this.searchContent}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                },
                (err)=>
                {
                    console.log(err);
                }
            )
        },

        search2:function () {
            this.$http.post('SearchByTag?type=master',{content:$('#select2').val()}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                }
            )
        },

        addList:function ()     //寻找主任
        {
            layer.open({
                type: 2,
                title: '寻找失主',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'addThing?type=master',
                end: function () {
                    location.reload();
                }
            });
        },

        getByTag:function (tag) {
            this.$http.post('SearchByTag?type=masterByTagName',{content:tag}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.radarList=[];
                    this.currentData=[];


                    this.radarList=data.body;
                    var len=this.radarList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.radarList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                }
            )
        },
        getDetail:function (radar_id) {
            layer.open({
                type: 2,
                title: '物品详情',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'thingDetail?type=master&radar_id='+radar_id
            });
        },
    },
    created:function () {
        this.$http.get('RadarListJson?type=master').then(
            (data)=>
            {
                this.pages=[];

                this.radarList=data.body;
                var len=this.radarList.length;
                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            }
        );
        this.$http.get('TagList').then(
            (data)=>
            {
                this.tagName=data.body;

                setTimeout(function () {
                    $(".chosen-select").chosen({
                        no_results_text: "没有找到结果！",//搜索无结果时显示的提示
                        search_contains:true,   //关键字模糊搜索，设置为false，则只从开头开始匹配
                        allow_single_deselect:true, //是否允许取消选择
                    })
                },'800');
            }
        );
    }
});

var tagTitle=new Vue({
    el:"#tagTitle",
    data:{
        
    },
    methods: {
        refresh:function () {
            setTimeout(
                function () {
                    var chosen=$(".chosen-select");
                    chosen.chosen("destroy").init();
                    chosen.chosen({
                        no_results_text: "没有找到结果！",//搜索无结果时显示的提示
                        search_contains:true,   //关键字模糊搜索，设置为false，则只从开头开始匹配
                        allow_single_deselect:true, //是否允许取消选择
                    })
                },200)


        }
    }
    
       
})

var my=new Vue({
    el:"#my",
    data:{
        radarList:[],
        currentData:[],    //当前数据列表
        each:6, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数
        type:'thing',
    },
    methods:{

        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
            {
                this.currentData.push(this.radarList[i]);
            }
        },

        //确认完成操作
        confirm:function (radar_id)
        {
            this.$http.post('Confirm',{radar_id:radar_id}).then(
                (data)=>
                {
                    if(data.body.result==='success')
                    {
                        layer.msg('操作完成');
                        location.reload();
                    }else
                    {
                        layer.msg('操作失败');
                    }
                }
            );
        }

    },
    created:function () {
        this.$http.get('RadarListJson?type=my').then(
            (data)=>
            {
                this.pages=[];

                this.radarList=data.body;

                var len=this.radarList.length;

                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.radarList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            }
        );


    }
});
