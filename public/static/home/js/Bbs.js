//获取get参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
var page=getQueryString("page");
var BbsContent=$("#BbsContent");
var UserData;
//打开新增留言页面
$('#add').on('click',function () {
    layer.open({
        type:2,
        title:"留个言",
        shadeClose:false,
        shade:0.8,
        area:["70%","70%"],
        content:"AddBbs.html",
        end:function () {
            location.reload();
        }
    })
});
//获取用户信息
$.ajax({
    url:"../Home/UserDataJson",
    async:false,
    dataType:"JSON",
    success:function (data) {
        UserData=data;
    },
    error:function () {
        layer.msg("数据获取错误");
    }
})
//分页
$.ajax({
    url:"PageNum",
    async:false,
    dataType:"JSON",
    success:function (data) {
        var PageNum=data.num;
        var Page=$("#Page");    //选择器
        var Html="";
        if(page==null||page==="1")
        {
            Html+='<li class="disabled"><span>«</span></li>';
        }else{
            Html+='<li><a href="?page=1">«</a></li>';
        }
        for(var i=1;i<=PageNum;i++)
        {
            if(i==page)
            {
                Html+='<li class="active"><span>'+i+'</span></li>';
            }else{
                Html+='<li><a href="?page='+i+'">'+i+'</a></li>';
            }
        }

        if (page===PageNum)
        {
            Html+='<li class="disabled"><span>»</span></li>'
        }else{
            Html+='<li><a href="?page="'+PageNum+'>»</a></li>'
        }
        Page.html(Html);
    },
    error:function () {
        layer.msg("数据获取错误");
    }
})
//获取动态
$.ajax({
    url:"BbsJson",
    type: "POST",
    dataType: "JSON",
    data:{
        'page':page
    },
    success:function (data) {
        var Html='';
        $.each(data,function (index,datum) {
            Html+=' <div class="social-feed-box" id="BbsContent">';
            Html+='<div class="social-avatar">\n' +
                '                        <a href="" class="pull-left">\n' +
                '                            <img alt="image" class="img-circle" src="http://q1.qlogo.cn/g?b=qq&nk='+datum.qq+'&s=160">\n' +
                '                            <!--qq-->\n' +
                '                        </a>\n' +
                '                        <div class="media-body">\n' +
                '                            <a href="../Home/UserIndex.html?uid='+datum.uid+'">\n' +
                '                                '+datum.name+'\n' +
                '                            </a>\n' +
                '                            <small class="text-muted text-success">'+datum.time+'</small>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="social-body">\n' +
                '                        <p>'+datum.content+'</p>\n' +
                '\n' +
                '                        <div class="btn-group">\n' +
                '                            <form action="" method="get" id="form'+datum.Id+'"></form>\n' +
                '                            <button class="btn btn-white btn-xs" onclick="show('+datum.Id+')">' +
                '                               <i class="fa fa-comments"></i>评论\n' +
                '                            </button>\n' +
                '                            <button class="btn btn-white btn-xs"><i class="fa fa-share"></i> 分享</button>\n' +
                '                            <button class="btn btn-white btn-xs" onclick="DeleteBbs('+datum.Id+')"><i\n' +
                '                                    class="fa fa-times"></i> 删除\n' +
                '                            </button>\n' +
                '\n' +
                '                        </div>\n' +
                '                    </div>';
            Html+='<div class="social-footer" id="text'+datum.Id+'" hidden>\n';
            $.ajax({
                async:false,
                url:"CommentJson",
                type:"POST",
                dataType:"JSON",
                data:{
                    BbsId:datum.Id
                },
                success:function (CommentData) {
                    $.each(CommentData,function (CommentIndex,CommentDatum) {
                        Html+='<div class="social-comment">\n' +
                            '                            <a href="" class="pull-left">\n' +
                            '                                <img alt="image" class="img-circle" src="http://q1.qlogo.cn/g?b=qq&nk='+CommentDatum.qq+'&s=160">\n' +
                            '                            </a>\n' +
                            '                            <div class="media-body">\n' +
                            '                                <p><a href="#">'+CommentDatum.name+'</a></p>\n' +
                            '                                '+CommentDatum.content+'\n' +
                            '                                <i onclick="DeleteComment('+CommentDatum.Id+')" style="color: red" class="pull-right fa fa-times"></i>\n' +
                            '                                <br>\n' +
                            '                                <small class="text-muted">'+CommentDatum.time+'</small>\n' +
                            '                            </div>\n' +
                            '                        </div>';
                    })
                },
                error:function () {
                    layer.msg("获取评论失败");
                }
            })

            Html+='                        <div class="social-comment" >\n' +
                '                            <a href="" class="pull-left">\n' +
                '                                <img alt="image" class="img-circle"\n' +
                '                                     src="http://q1.qlogo.cn/g?b=qq&nk='+UserData.qq+'&s=160">\n' +
                '                            </a>\n' +
                '                            <div class="media-body">\n' +
                '                                <textarea class="form-control" placeholder="填写评论..." id="comment'+datum.Id+'" name="info"></textarea>\n' +
                '                            </div>\n' +
                '                            <div align="right">\n' +
                '                                <button type="button" onclick="SendComment('+datum.Id+')" class="btn btn-primary btn-xs">\n' +
                '                                    <i class="fa fa-check"></i>发表\n' +
                '                                </button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>';
            Html+='</div>';
        });
        BbsContent.html(Html);
    },
    error:function () {
        layer.msg("动态获取失败");
    }
});
function show(Id)   //显示评论
{
    var text=$("#text"+Id);
    text.toggle(500);
}
function DeleteBbs(Id)  //删除留言
{
    $.ajax({
        url:"DeleteBbs",
        type:"POST",
        dataType:"JSON",
        data:{
            Id:Id
        },
        success:function (data) {
            if(data.result==="success")
            {
                layer.msg("删除成功");
                setTimeout(function () {
                    location.reload();
                },2000);
            }else{
                layer.msg(data.msg);
            }
        },
        error:function () {
            layer.msg("删除失败");
        }
    })
}
function DeleteComment(Id)  //删除评论
{
    $.ajax({
        url:"DeleteComment",
        type:"POST",
        dataType:"JSON",
        data:{
            Id:Id
        },
        success:function (data) {
            if(data.result==="success")
            {
                layer.msg("删除成功");
                setTimeout(function () {
                    location.reload();
                },2000);
            }else{
                layer.msg(data.msg);
            }
        },
        error:function () {
            layer.msg("删除失败");
        }
    })
}
function SendComment(Id) {
    var content=$("#comment"+Id).val();
    $.ajax({
        url:"SendComment",
        type:"POST",
        dataType:"JSON",
        data:{
            content:content,
            Upper:Id
        },
        success:function (data) {
            if (data.result==="success"){
                layer.msg("评论成功");
                setTimeout(function () {
                    location.reload();
                },2000);
            }else{
                layer.msg(data.msg);
            }
        },
        error:function () {
            layer.msg("评论失败");
        }
    })
}