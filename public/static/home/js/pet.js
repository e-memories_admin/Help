var adobt=new Vue({
    el:"#adobt",
    data:{
        petList:[],
        currentData:[],    //当前数据列表
        each:6, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数


        tagName:[], //标签名称列表
        searchContent:'',
    },
    methods:{

        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.petList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.petList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
            {
                this.currentData.push(this.petList[i]);
            }
        },

        search:function ()
        {
            this.$http.post('PetListJson',{content:this.searchContent}).then(
                (data)=>
                {
                    //初始化
                    this.pages=[];
                    this.petList=[];
                    this.currentData=[];


                    this.petList=data.body;
                    var len=this.petList.length;
                    this.endPage=Math.ceil(len/this.each);

                    //初始化页面显示第一页内容
                    for (i=0;i<Math.min(this.each,len);i++)
                    {
                        this.currentData.push(this.petList[i]);
                    }

                    //生成可见的页码的列表
                    for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                    {
                        this.pages.push(i);
                    }
                },
                (err)=>
                {
                    console.log(err);
                }
            );
        },

        addList:function ()     //发布领养信息
        {
            layer.open({
                type: 2,
                title: '发布领养信息',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'addPet',
                end: function () {
                    location.reload();
                }
            });
        },

        getDetail:function (pet_id) {
            layer.open({
                type: 2,
                title: '宠物详情',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: 'petDetail?pet_id='+pet_id
            });
        },
    },
    created:function () {
        this.$http.get('PetListJson').then(
            (data)=>
            {
                this.pages=[];

                this.petList=data.body;

                var len=this.petList.length;
                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.petList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            }
        );
    }
});

var my=new Vue({
    el:"#my",
    data:{
        petList:[],
        currentData:[],    //当前数据列表
        each:6, //每页数据个数
        startPage:1,    //起始页
        endPage:1,      //结束页
        currentPage:1,   //当前页面
        pages:[1],  //当前显示的页数列表
        enablePage:5,    //运行显示的页数
        type:'thing',
    },
    methods:{

        //下一页的操作
        add:function () {
            if(this.currentPage<this.endPage)
            {
                this.currentPage++; //页数加一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.splice(0,1);
                    this.pages.push(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.petList[i]);
                }

            }

        },

        //上一页操作
        minus:function () {
            if(this.currentPage>this.startPage)
            {
                this.currentPage--; //页数减一

                //防止页码不显示
                if(!this.pages.includes(this.currentPage))
                {
                    this.pages.pop();
                    this.pages.unshift(this.currentPage);
                }

                //更新当前页面的数据
                this.currentData=[];
                for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
                {
                    this.currentData.push(this.petList[i]);
                }
            }
        },

        //跳转页面操作
        jump:function (page) {

            if (page>this.currentPage)
            {
                for(i=this.currentPage+1;i<=page;i++)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.splice(0,1); //弹出首元素
                        this.pages.push(i);
                    }
                }

            }
            else
            {
                for(i=this.currentPage-1;i>=page;i--)
                {
                    if(!this.pages.includes(i))
                    {
                        this.pages.pop();
                        this.pages.unshift(i);
                    }
                }

            }


            this.currentPage=page;
            //更新当前页面的数据
            this.currentData=[];
            for (i=(this.currentPage-1)*this.each;i<Math.min((this.currentPage)*this.each,this.radarList.length);i++)
            {
                this.currentData.push(this.petList[i]);
            }
        },

        //确认完成操作
        confirm:function (pet_id)
        {
            this.$http.post('Confirm',{pet_id:pet_id}).then(
                (data)=>
                {
                    if(data.body.result==='success')
                    {
                        layer.msg('操作完成');
                        location.reload();
                    }else
                    {
                        layer.msg('操作失败');
                    }
                }
            );
        }

    },
    created:function () {
        this.$http.get('PetListJson?type=my').then(
            (data)=>
            {
                this.pages=[];

                this.petList=data.body;

                var len=this.petList.length;

                this.endPage=Math.ceil(len/this.each);

                //初始化页面显示第一页内容
                for (i=0;i<Math.min(this.each,len);i++)
                {
                    this.currentData.push(this.petList[i]);
                }

                //生成可见的页码的列表
                for(i=this.startPage;i<=Math.min(this.endPage,this.enablePage);i++)
                {
                    this.pages.push(i);
                }
            }
        );


    }
});
