$(function(){
	//全选
	$("#chkall").click(function(){
			$("input[name='chk[]']").attr("checked",$(this).attr("checked"));
	})
	
	//弹出删除提示
	$("form[name='listForm']").submit(function(){
		if($("input[name='chk[]']:checked").size() == 0)
		{
			alert("你没有选择任何内容！");
			return false;
		}
		if(confirm("你确定要删除吗？")){
			return true;
		}else{
			return false;
		}
	})
	

	$("#filename_cn").live("keyup keydown change blur",function (){
		$("#filename_en").val($(this).toPinyin().replace(" ", ""));
	 });

	
	/*隔色*/
	$(".table-data tr").hover(function(){
		$(this).addClass("bgFleet");
	},function(){
		$(this).removeClass("bgFleet");
	})
	
	//表单验证
	$("input[type='submit']").click(function()
	{
		message = ''
		$("input").each(function(){
			if($(this).attr("msg") != "" && typeof($(this).attr("msg"))!="undefined" && $(this).val() == "")
			{
				message = message + '- ' +($(this).attr("msg")) + '\n';
			}
		  });	
		if(message != '')
		{
			alert(message);	
			return false;
		}else
		{
			return true;
		}
	})
	
})

//检查表单
function chk(e)
{
	var values, index;	
	values = $("#CKFORM").serializeArray();
	alert(values);
	for (index = 0; index < values.length; ++index)
	{
		if (values[index].name == "com_name")
		{
			alert("hello");
		}
	}
	return false;
}

//删除图片
function del_pic(table, id)
{
	if(confirm("你确定要删除此图吗？"))
	{
		$.ajax({
			type: "GET",
			url: "ajax.php",
			data: {act:'del_pic', table:table, id:id},
			dataType: "json",
			success: function(data){
			   alert(data.message);
			   if(data.error == 0)
			   {
			  	 $('#PICTURE').empty();
			   }
			}
		});
	}
}

/*  
type   页面
status 状态
*/
function toggleItem(element, type, id)
{
	if($(element).attr("src") == 'images/yes.gif')
	{
		status = 'close';
	}else
	{
		status = 'open';	
	}
	$.ajax({
		type: 'GET',
		url : 'ajax.php',
		data: {act:'toggle', type:type, status:status, id:id},
		dataType: 'json',
		success: function(result){
			if(result.error == 0)
			{
				$(element).attr('src', result.src);
			}
		}
	})	
	
	
}

/*  
type   页面
status 状态
*/
function toggleItem2(val, type, id)
{
/*	if(val == 3 && type == 'user')
	{
		if($('.user_' + id).find(".code").val()  == '')
		{
			alert('请先生成验证码！');
			return false;
		}
	}*/
	$.ajax({
		type: 'GET',
		url : 'ajax.php',
		data: {act:'toggle', type:type, status:val, id:id},
		dataType: 'json',
		success: function(result){
			if(result.error == 0)
			{
				window.location.reload();
			}
		}
	})	

}

/*  
type   页面
status 状态
*/
function updateCode(element, user_id)
{

	e = $(element);
	$.ajax({
		type: 'GET',
		url : 'ajax.php',
		data: {act:'update_code', code:e.find(".code").val(), user_id:user_id},
		dataType: 'json',
		success: function(result){
			if(result.error == 0)
			{
				alert("更新成功");
			}
		}
	})	

}