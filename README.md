湖工志愿服务平台
===============

湖北工程学院志愿者服务平台是一个面向于湖北工程学院的志愿者平台，后续可以开拓至全国高校

此平台归属于湖北工程学院计算机学院青协管理，却又不只是为青协服务，他创办的初衷是打造一个便捷全校学生生活与学习相关需要的平台，其主要功能如下：

 + 1、注册登录
 + 2、通过找回密码
 + 3、绑定QQ并通过QQ登录
 + 4、用户管理
 + 5、发送与查看内部通知
 + 6、**导入、查询无课表**
 + 7、编辑青协人员的个人描述
 + 8、**留言板**
 + 9、**召唤志愿者**
 + 10、青协历届干事（青协荣誉榜）
 + 11、**寻物雷达**（就是寻物启事以及寻找失主）
 + 12、**成绩大比拼**（对接教务系统并获取成绩，将网站内的同学一起进行排名比拼，促进学习）
 + 13、**宠物领养**
 + 14、**心愿墙**
 + 15、bug与建议反馈
 + 16、**查询空教室**
 + 17、批量发送招新录取短信
 + 18、**活动管理**
 + 更多功能正在更新...


> 程序为网站，并开放相关接口，要求运行服务器为
>
> php>7.0
>
> mysql>5.1
>
> apache
>
> 无需特别运行环境


## 目录结构

初始的目录结构如下：

~~~
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  ├─module_name        模块目录
│  │  ├─common.php      模块函数文件
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─view            视图目录
│  │  └─ ...            更多类库目录
│  │
│  ├─command.php        命令行定义文件
│  ├─common.php         公共函数文件
│  └─tags.php           应用行为扩展定义文件
│
├─config                应用配置目录
│  ├─module_name        模块配置目录
│  │  ├─database.php    数据库配置
│  │  ├─cache           缓存配置
│  │  └─ ...            
│  │
│  ├─app.php            应用配置
│  ├─cache.php          缓存配置
│  ├─cookie.php         Cookie配置
│  ├─database.php       数据库配置
│  ├─log.php            日志配置
│  ├─session.php        Session配置
│  ├─template.php       模板引擎配置
│  └─trace.php          Trace配置
│
├─route                 路由定义目录
│  ├─route.php          路由定义
│  └─...                更多
│
├─public                WEB目录（对外访问目录）
│  ├─index.php          入口文件
│  ├─upload	        照片上传目录
│  ├─images	       另一个照片上传目录
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─thinkphp              框架系统目录
│  ├─lang               语言文件目录
│  ├─library            框架类库目录
│  │  ├─think           Think类库包目录
│  │  └─traits          系统Trait目录
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  ├─phpunit.xml        phpunit配置文件
│  └─start.php          框架入口文件
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~

## 详细介绍

以下具体对重点功能进行介绍

### 内部通知

通过选项可对平台里面特定身份群体用户进行消息通知，一是完成了批量的通知，二是邮箱比艾特群成员效果更好

###查询空教室

通过选择学年、学期、周、日、节次就可以直接查询空教室，而且无需登录

### 导入、查询无课表

对接了超级课程表的平台，实现了一次导入，无需二次更新，只需在自己的超级课程表中更新即可，大大提高了办事效率，省去了往常制作无课表的繁杂步骤，且无课表精准到每一节课，预防了往常制作过程中的失误以及中途调课的尴尬

### 对接教务系统

输入账号密码即可对接教务系统，完成对接操作以后，可以通过教务系统来更新无课表（时效性比较强），同时可以进行成绩查询和成绩通知的功能（后台默认会查询并发送到邮箱）

### 活动管理、活动报名

为青协活动量身定制，可以为各院青协提供便捷操作，并规范活动流程。在活动管理中添加新活动以后，点击管理，可对每个活动进行管理，在信息收集的步骤，指定用户可以报名参与开展的活动，活动结束以后，活动参与人员自行填写志愿汇表，后台会自动生成相关文件

### 留言板

留言板类似QQ空间，但是他更像一个校园的内部交流平台，任何事情都可以在上面发布，也可以进行评论，更多的，发布内容是可以由用户自己定义样式的，很是方便

### 召唤志愿者

往常在志愿活动之中，经常会出现一些摆摊无人问津，寻求帮助却又找不着人的情况，根据分析，这个大多是由于信息不通造成的，即便创建了帮助群，依然会有一些志愿者因为过多的无用消息将群屏蔽，平台使用邮件通知志愿者，并可以由同学自主选择求助对象，极大得缓解这一系列的问题

### 寻物雷达

个人发现，校园内丢失东西和捡到东西的情况时有发生，但是校园内的微服务平台，类似于章鱼墙、小黑板等等，发布信息过于杂乱，很难有聚集化的信息，而且QQ空间也并不具备搜索功能，即使有，失主也不一定会关注所有的微服务平台，这就为失主找回物品造成极大的困扰，有时候，即使捡到东西的同学在各个墙上发布信息寻找失主，也很难找到失主，分析其根本，就是信息复杂化，分散化造成的困扰，因此有这样一个独立的失物平台就显得尤为重要，平台将失物分为两类，一种是捡到的同学可以在上面发布寻找主人的信息，另一种是失主可以发布寻回信息，并加上悬赏内容，这样可以极大的提高失物归主的概率

### 成绩大比拼

在学校中，学习氛围明显不如高中，其中很重要的一点原因是，大家都不再认为分数十分重要，这样一来，少了竞争，学习兴趣很难上来，但是又有这么一批大学生，互相比较学习，互相进步，以提高学习水平为目的，但是奈何缺少比拼，学习兴趣难以上来，因此，平台对接了教务系统，可以查询每学期每门课程在网站内的排名，而这个排名又不会公开，既保全了自己的隐私，又能知道自己的水平层次，极大提高学习兴趣

### 宠物领养

大学以后，不少同学开始拥有自己的宠物（当然该行为不值得提倡），但是大家难免有个问题，就是碰到放假或者毕业时，家离学校较远，飞机高铁又不能带宠物，快递运输又会对宠物造成不小的伤害，甚至是造成小动物的死亡，因此，许多大学生会选择将宠物托付给一个新主人，但是很难联系到，这样一个平台就能解决这个问题，为宠物领养提供了便利

### 心愿墙

匿名发送心愿，随机显示心愿，大家心情苦闷之时，也可在上面倾诉，而且不用担心被人认出

## 版权信息

版权归湖工计科王宇哲及其团队所有，一切使用需要遵循本人及其团队成员同意

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。
