<?php

namespace app\robot;

class Zfsoft
{

    /**
     * @var string 获取cookie的URL
     */
    private $CookieUrl="http://zfsoft.e-memories.cn/schedule";

    /**
     * @var string 获取课表的URL
     */
    private $CourseUrl="http://61.183.22.151:8080/jwglxt/kbcx/xskbcx_cxXsKb.html?gnmkdm=N2151&su=";

    /**
     * @var string 获取成绩的URL
     */
    private $ScoreUrl="http://61.183.22.151:8080/jwglxt/cjcx/cjcx_cxDgXscj.html?doType=query&gnmkdm=N305005&su=020301700164";

    /**
     * @var string 获取楼号对应代码的接口
     */
    private $BuildingUrl="http://61.183.22.151:8080/jwglxt/cdjy/cdjy_cxXqjc.html?gnmkdm=N2155&xqh_id=00001&xnm=2021&xqm=3";

    /**
     * @var string 查询空教室的URL
     */
    private $RoomUrl="http://61.183.22.151:8080/jwglxt/cdjy/cdjy_cxKxcdlb.html?doType=query&gnmkdm=N2155";

    /**
     * @var string[] 公用访问头文件
     */
    private $header=[
        "Accept: */*",
        "Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "Cache-Control: no-cache",
        "Content-Type: application/x-www-form-urlencoded",
        "Origin: http://61.183.22.151:8080",
        "Pragma: no-cache",
        "Proxy-Connection: keep-alive",
        "Host:61.183.22.151:8080",
        "User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Mobile Safari/537.36 Edg/95.0.1020.53",
        "X-Requested-With: XMLHttpRequest"
    ];

    /**
     * @var string 学号
     */
    private $sid;

    /**
     * @var string 密码
     */
    private $password;

    /**
     * @var String 访问教务系统的Cookie
     */
    private $JSESSIONID;


    public function __construct($sid,$password)
    {
        $this->sid=$sid;
        $this->password=$password;
        //获取Cookie
        $this->JSESSIONID=$this->getJSESSIONID();
    }


    /**
     * 带Cookie模拟访问地址并解析json为数组返回
     * @param string $url 访问地址
     * @param array $param 访问参数
     * @return mixed 结果数组
     */
    private function curlPost($url="",$param=[]){


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($param));
        curl_setopt($ch,CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($ch,CURLOPT_COOKIE,'JSESSIONID='.$this->JSESSIONID);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response,true);
    }

    /**
     * 获取COOKIE
     * @return String COOKIE
     */
    public function getJSESSIONID(){

        $header=[
            "Accept:application/json, text/javascript, */*; q=0.01",
            "Accept-Language:zh-CN,zh;q=0.9",
            "Connection:keep-alive",
            "Host:zfsoft.e-memories.cn",
            "User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
            "X-Requested-With:XMLHttpRequest"
        ];

        //构建请求地址
        $url=$this->CookieUrl."?password=".$this->password.'&sid='.$this->sid;

        //curl模拟访问
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        $output = curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);
        return $output['JSESSIONID'];

    }

    /**
     * 获取成绩
     * @param string $xnm 学年（2020-2021填2020）
     * @param string $xqm 学期名(3:第一学期，12：第二学期)
     * @return mixed 成绩
     */
    public function getScore($xnm='2021',$xqm='3'){

        $url=$this->ScoreUrl;

        //构造参数用于传递
        $param=[
            'xnm'=>$xnm,
            'xqm'=>$xqm,
            'kzlx'=>'ck'
        ];

        //带着COOKIE模拟访问获取成绩
        return $this->curlPost($url,$param);

    }

    /**
     * 获取成绩
     * @param string $xnm 学年（2020-2021填2020）
     * @param string $xqm 学期名(3:第一学期，12：第二学期)
     * @return mixed 成绩
     */
    public function getSchedule($xnm='2021', $xqm='3'){

        $url=$this->CourseUrl;

        //构造参数用于传递
        $param=[
            'xnm'=>$xnm,
            'xqm'=>$xqm,
        ];

        //带着COOKIE模拟访问获取课程表
       return $this->curlPost($url,$param);

    }

    /**
     * 获取空教室信息
     * @param array $week 哪几周
     * @param array $day 星期几
     * @param array $time 哪几节课
     * @param string $xnm 哪一学年
     * @param string $xqm 哪一学期
     * @param string $xqh_id 校区id
     * @return mixed 空教室信息数组
     */
    public  function getRoom($week=0,$day=[],$time=0,$xnm='2021',$xqm='3',$lh='',$xqh_id='00001'){

        //        //选中的周，第几节课，0为什么都没选
        //        $selectWeek=0;
        //        $selectTime=0;
        //
        //
        //        //TODO 前端已经完成了进制采集数据，后端直接接收使用即可
        //        //教务系统采用二进制来判断选中的周和第几节课
        //        //比如1001表示选中了第1和第4节，所以用数字9代替
        ////        foreach ($week as $value){
        ////            $selectWeek+=(int)(pow(2,(int)$value-1));
        ////        }
        ////        foreach ($time as $value){
        ////            $selectTime+=(int)(pow(2,(int)$value-1));
        ////        }

        $selectDay="";

        //将字符串用逗号连接起来
        if (isset($day)){
            $selectDay=implode(',',$day);
        }

        $url=$this->RoomUrl;

        $param=[
            'fwzt'=>'cx',
            'xqh_id'=>$xqh_id,
            'xnm'=>$xnm,
            'xqm'=>$xqm,
            'cdlb_id'=>'',
            'cdejlb_id'=>'',
            'qszws'=>'',
            'jszws'=>'',
            'cdmc'=>'',
            'lh'=>$lh,
            'jyfs'=>'0',
            'cdjylx'=>'',
            'zcd'=>$week,
            'xqj'=>$selectDay,
            'jcd'=>$time,
            '_search'=>'false',
            'nd'=>time(),
            'queryModel.showCount'=>10000,
            'queryModel.currentPage'=>1,
            'queryModel.sortName'=>'cdbh',
            'queryModel.sortOrder'=>'asc',
            'time'=>'1'
        ];

        return $this->curlPost($url,$param);

    }

    /**
     * 获取楼号信息
     * @param string $xnm 哪一学年
     * @param string $xqm 哪一学期
     * @param string $xqh_id 校区id
     * @return mixed 空教室信息数组
     */
    public  function getBuilding($xnm='2021',$xqm='3',$xqh_id='00001'){


        $url=$this->BuildingUrl."?xnm=".$xnm."&xqm=".$xqm;

        $BuildingData=$this->curlPost($url);

        return $BuildingData['lhList'];
        

    }

}