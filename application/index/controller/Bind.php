<?php

namespace app\index\controller;
use think\Db;

class Bind extends \think\Controller
{
    public function bind()   //绑定QQ
    {
        return $this->fetch('index/bind');
    }
    public function BindJson()    // 绑定接口
    {
        $openid = \session('openid');

        $User=Db::name('users')->where(['username'=>input('post.name'),'password'=>md5(input('post.password'))])->find();
        if (!empty($User)) {
            if($User['active']==0){
                return json(['result'=>'error','msg'=>'账号还未激活']);
            }else{
                $SafeCode = GetRandStr(20);
                $result=Db::name('users')->where(['username'=>input('post.name'),'password'=>md5(input('post.password'))])
                    ->update(['SafeCode'=>$SafeCode,'openid'=>$openid]);
                cookie('SafeCode', $SafeCode);
                if ($result)
                {
                    Monitor('绑定了qq',$SafeCode);
                    return json(['result'=>'success','SafeCode'=>$SafeCode]);
                }else{
                    return json(['result'=>'error','msg'=>'未知错误']);
                }
            }

        } else {
            return json(['result'=>'error','msg'=>'用户名或密码错误']);
        }
    }
}
