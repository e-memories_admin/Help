<?php

namespace app\office\controller;

use think\Controller;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\DbException;
use think\exception\PDOException;
use think\response\Json;

/**
 * 参加活动类
 */
class Join extends Controller
{

    /**
     * 活动列表
     * @return mixed
     */
    public function ActivityList(){
        return $this->fetch('join/ActivityList');
    }

    /**
     * 活动列表接口
     * @return Json
     */
    public function ActivityListJson()
    {
        //获取安全码
        $SafeCode=cookie('SafeCode');

        //获取我的学院名称
        $myCollege=db('users')->where('SafeCode',$SafeCode)->value('college');

        try {
            $activityData = db('activity')->alias('a')
                ->leftJoin('wyz_users u', 'a.uid=u.Id')
                ->leftJoin('wyz_college c', 'a.college_id=c.Id')
                ->field('a.*,u.name as creator,c.college_name')
                ->where(['c.college_name'=>$myCollege,'a.toAll'=>'0'])
                ->whereOr('a.toAll','1')
                ->select();
        }catch (Exception $e) {

            //返回错误信息
            return json(['result'=>'error','msg'=>'数据库错误']);
        }

        //使用循环更改查出来的步骤的值，生成字符串，减轻前端压力
        foreach($activityData as $key=>$activityDatum){
            if($activityDatum['common']=='1'){
                switch ($activityDatum["step"]){
                    case '1':
                        $activityData[$key]["step"]="活动准备中";
                        break;
                    case '2':
                        $activityData[$key]["step"]="活动报名中";
                        break;
                    case '3':
                    case '4':
                        $activityData[$key]["step"]="活动待开展";
                        break;
                    case '5':
                        //同时进行补录或者生成时长，由参与者填写
                        $activityData[$key]["step"]="正在收集信息";
                        break;
                    case '6':
                    case '7':
                        $activityData[$key]["step"]="活动完成";
                        break;
                }
            }else{
                switch ($activityDatum["step"]){
                    case '1':
                        $activityData[$key]["step"]="活动报名中";
                        break;
                    case '2':
                    case '3':
                        $activityData[$key]["step"]="活动待开展";
                        break;
                    case '4':
                        //同时进行补录或者生成时长，由参与者填写
                        $activityData[$key]["step"]="收集信息中";
                        break;
                    case '5':
                        $activityData[$key]["step"]="活动完成";
                        break;
                }
            }
        }


        //返回查询数据
        return json($activityData);
    }


    /**
     * 活动详情页面
     * @return mixed
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function ActivityDetail(){

        //获取当前用户Id
        $SafeCode=cookie('SafeCode');
        $uid=db('users')->where('SafeCode',$SafeCode)->value('Id');

        //获取活动Id
        $ActivityId=input('get.Id');

        //查询用户与活动的报名关系
        $userActivity=db('user_activity')->where([
            'uid'=>$uid,
            'activity_id'=>$ActivityId
        ])->find();

        //如果不存在报名关系
        if (empty($userActivity)){
            $isRegistered=false;
        }else{
            $isRegistered=true;
        }

        $this->assign('isRegistered',$isRegistered);


        //活动信息
        $ActivityData=db('activity')->alias('a')
            ->leftJoin('wyz_users u','u.Id=a.uid')
            ->leftJoin('wyz_college c','c.Id=a.college_id')
            ->field('a.*,c.college_name,u.name as username')
            ->where('a.Id',$ActivityId)->find();

        //懒得改代码了
        $ActivityData["step_id"]=$ActivityData["step"];

        if($ActivityData['common']=='1'){
            switch ($ActivityData["step"]){
                case '1':
                    $ActivityData["step"]="活动准备中";
                    break;
                case '2':
                    $ActivityData["step"]="活动报名中";
                    break;
                case '3':
                case '4':
                    $ActivityData["step"]="活动待开展";
                    break;
                case '5':
                    //同时进行补录或者生成时长，由参与者填写
                    $ActivityData["step"]="正在收集信息";
                    break;
                case '6':
                case '7':
                    $ActivityData["step"]="活动完成";
                    break;
            }
        }else{
            switch ($ActivityData["step"]){
                case '1':
                    $ActivityData["step"]="活动报名中";
                    break;
                case '2':
                case '3':
                    $ActivityData["step"]="活动待开展";
                    break;
                case '4':
                    //同时进行补录或者生成时长，由参与者填写
                    $ActivityData["step"]="收集信息中";
                    break;
                case '5':
                    $ActivityData["step"]="活动完成";
                    break;
            }
        }


        //信息传入模板
        $this->assign("ActivityData",$ActivityData);

        if($ActivityData["common"]=='1'){

            if ($ActivityData["step_id"]=='2'){
                //令详情页类型为1，显示报名按钮
                $this->assign('type','1');
            }else if($ActivityData["step_id"]=="5"){

                //令详情页类型为2，显示志愿时长填报框
                $this->assign('type','2');
            }else{
                //令详情页类型为3，只显示活动信息
                $this->assign('type','3');
            }

        }else{
            if ($ActivityData["step_id"]=="1"){

                //令详情页类型为1，显示报名按钮
                $this->assign('type','1');
            }else if($ActivityData["step_id"]=="4"){
                //令详情页类型为2，显示志愿时长填报框
                $this->assign('type','2');
            }else{
                //令详情页类型为3，只显示活动信息
                $this->assign('type','3');
            }
        }

        return $this->fetch("join/ActivityDetail");
    }

    /**
     * @return Json 结果
     */
    public function RegisterJson()
    {

        $ActivityId=input('post.ActivityId');

        $SafeCode=cookie('SafeCode');
        $uid=db('users')->where('SafeCode',$SafeCode)->value('Id');

        try{

            //尝试报名
            db('user_activity')->insert([
                'uid'=>$uid,
                'activity_id'=>$ActivityId
            ]);

            //报名人数加一
            db('activity')->where('Id',$ActivityId)->setInc('participant_num',1);

            return json(['result'=>'success']);
        }catch (Exception $e){

            return json(['result'=>'error','msg'=>$e->getMessage()]);
        }

    }

    /**
     * 提交志愿时长表
     * @throws DataNotFoundException
     * @throws PDOException
     * @throws DbException
     * @throws ModelNotFoundException
     * @throws Exception
     */
    public function ConfirmTime() {
        $SafeCode=cookie('SafeCode');

        $uid=db('users')->where('SafeCode',$SafeCode)->value('Id');
        $ActivityId=input('get.ActivityId');

        $ActivityData=db('voluntary')->where('Id',$ActivityId)->find();

        $isExist=db('user_activity')->where([
            'uid'=>$uid,
            'activity_id'=>$ActivityId
        ])->find();

        if (empty($isExist)){
            $this->error("操作失败，你没有报名这个活动哦");
            return;
        }

        $input=input('post.');

        $time=$input['time'];
        $append= $input['append'] ?? '0';

        db('user_activity')->where([
            'uid'=>$uid,
            'activity_id'=>$ActivityId
        ])->update([
            'time'=>$time,
            'append'=>$append
        ]);

        return json(['result'=>'success']);


    }
}