<?php

namespace app\office\controller;

use think\Controller;
use think\facade\Env;

class Demo extends Controller
{
    public function demo(){
        $word=new Word();

        $file=Env::get('root_path')."public/";

        $word->substitute($file,[
            'name'=>'111',
            'sex'=>'男'
        ]);
    }
}