<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use think\Db;


function C($key)    //获取网站配置
{
    $data = Db::name('config')->where('key', $key)->find();
    return $data['value'];
}

function Monitor($msg,$SafeCode='')  //监控用户行为
{
    if ($msg=='')
        return false;

    if ($SafeCode=='')
        $SafeCode=cookie('SafeCode');
    $User=\db('users')->where('SafeCode',$SafeCode)->find();
    $uid=$User['Id'];

    $msg=$User['name'].$msg;

//    trace($msg,'info');

    \db('monitor')->insert([
        'msg'=>$msg,
        'uid'=>$uid,
        'time'=>date('Y-m-d H:i:s')
    ]);


}


/**
 * Notes:发送邮件
 * @param $tomail //收件人地址
 * @param $name //收件人名称
 * @param string $subject //主题
 * @param string $body //内容
 * @param null $attachment //附件
 * @return bool
 * @throws \PHPMailer\PHPMailer\Exception
 */
function send_mail($tomail, $name, $subject = '', $body = '', $attachment = null)
{

    $username = C('mail_username');
    $password = C('mail_password');
    $mail = new \PHPMailer\PHPMailer\PHPMailer();           //实例化PHPMailer对象
    $mail->CharSet = 'UTF-8';           //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
    $mail->IsSMTP();                    // 设定使用SMTP服务
    $mail->SMTPDebug = 0;               // SMTP调试功能 0=关闭 1 = 错误和消息 2 = 消息
    $mail->SMTPAuth = true;             // 启用 SMTP 验证功能
    $mail->SMTPSecure = 'ssl';          // 使用安全协议
    $mail->Host = "smtp.exmail.qq.com";        // 企业邮局域名
    $mail->Port = 465;                  //设置ssl连接smtp服务器的远程服务器端口号 可选465或587

    $mail->Username = $username;    //邮件发送人的用户名(请填写完整的email地址)
    $mail->Password = $password;    // 邮件发送人的 密码 （授权码）

    $mail->SetFrom($username, $username);
    $replyEmail = '';                   //留空则为发件人EMAIL
    $replyName = '';                    //回复名称（留空则为发件人名称）
    $mail->AddReplyTo($replyEmail, $replyName);  //回复的地址

    $mail->Subject = $subject;   //邮件标题
    $mail->MsgHTML($body);       //邮件内容

    $mail->AddAddress($tomail, $name);  //收件人地址，("收件人email","收件人姓名")

    if (is_array($attachment)) { // 添加附件
        foreach ($attachment as $file) {
            is_file($file) && $mail->AddAttachment($file);
        }
    }
    return $mail->Send() ? true : $mail->ErrorInfo;
}
function GetRandStr($len) //随机码生成函数
{
    $chars = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k","l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w", "x", "y", "z","0", "1", "2","3", "4", "5", "6", "7", "8", "9");
    $charsLen = count($chars) - 1;
    shuffle($chars);
    $output = "";
    for ($i=0; $i<$len; $i++){
        $output .= $chars[mt_rand(0, $charsLen)];
    }
    return $output;
}