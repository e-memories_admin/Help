<?php
namespace app\api\controller;
use app\home\controller\Robot;
use think\Db;

class Api extends \think\Controller
{
    public function qq_api()    //  qq登录接口
    {
        $qqauth = new \Yurun\OAuthLogin\QQ\OAuth2('101932555', 'bedc044973e286c18feaf8c4f04e4465', 'http://'.$_SERVER['HTTP_HOST'].'/api/Api/callback');
        $url = $qqauth->getAuthUrl();
        \session('YURUN_QQ_STATE', $qqauth->state);
        $this->redirect($url);
    }

    public function callback()  //QQ登录回调地址
    {
        $qqauth = new \Yurun\OAuthLogin\QQ\OAuth2('101932555', 'bedc044973e286c18feaf8c4f04e4465', 'http://'.$_SERVER['HTTP_HOST'].'/api/Api/callback');
        $accessToken = $qqauth->getAccessToken(\session('YURUN_QQ_STATE'));
        $userInfo = $qqauth->getUserInfo();
        $openid = $qqauth->openid;
        $nickname = $userInfo['nickname'];
        $gender = $userInfo['gender'];
        $local = $userInfo['province'] . $userInfo['city'];
        $birth_year = $userInfo['year'];
        $user = Db::name('users')->where('openid', $openid)->find();
        $SafeCode = GetRandStr(20);
        if (!empty($user)) {
            Db::name('users')->where('openid', $openid)->update(['SafeCode' => $SafeCode]);
            cookie('SafeCode', $SafeCode);

            Monitor('使用qq登录了!',$SafeCode);

            $this->success('登陆成功', 'home/Home/index');
        } else {
            \session('openid', $openid);
            \session('nickname', $nickname);
            \session('gender', $gender);
            \session('local', $local);
            \session('birth_year', $birth_year);
            $this->redirect('index/Bind/bind');
        }
    }

    /**
     * 成绩查询接口
     */
    public function scoreJson()
    {
        //成绩访问接口
        $Api=new Robot();

        $year=input('get.year');
        $term=input('get.term');

        $Sids=\db('sid')->select();
        //获取输入的年份和学期以确定输出的成绩数据是否为某学期的还是全部
        foreach ($Sids as $Sid)
        {

            if (input('get.year') && input('get.term')) {
                $Api->getScore($Sid['uid'],$Sid['sid'],$Sid['pwd'],$year,$term,true);
                $scores = \db('grade')->where(['uid' => $Sid['uid'], 'year' => $year, 'term' => $term])->select();
            }else{
                return json(['result'=>'error']);
            }


            $sum_num=0; //统计不同科目考试最大总人数
            //更新名称
            foreach ($scores as $score) {
                //找出所有比自己成绩高的同学的成绩的个数以确定自己的名次（每一科）
                $num = \db('grade')->where( 'courseCode',$score['courseCode'])
                    ->where('uid', '<>', $score['uid'])
                    ->where('score', '>', $score['score'])
                    ->where(['year' => $score['year'], 'term' => $score['term']])
                    ->count();

                //当前科目的总人数
                $nums = \db('grade')->where('courseCode',$score['courseCode'])
                    ->where(['year' => $score['year'], 'term' => $score['term']])
                    ->count();

                $sum_num=max([$nums,$sum_num]);

                //更新每一科的名次
                \db('grade')->where('uid', $score['uid'])->where('courseCode', $score['courseCode'])->update(['order' => $num + 1]);
            }


        }
        return json(['result'=>'complete']);
    }
}
