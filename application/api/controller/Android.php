<?php
namespace app\api\controller;

use think\Controller;
use think\Db;

class Android extends Controller{
//    public function Login(){
//        $username=input('param.username');
//        $password=input('param.password');
//
//        if (empty($username)||empty($password))
//            return json(['result'=>'error','msg'=>'输入不完整']);
//        $UserData=db('users')->where(['username'=>$username,'password'=>md5($password)])->find();
//        if (empty($UserData))
//            return json(['result'=>'error','msg'=>'密码错误']);
//
//
//
//        return json(['result'=>'success','msg'=>'登陆成功','SafeCode'=>$UserData['SafeCode']]);
//    }

    public function Login()  //绑定接口
    {
        $data=input('param.');
        $username=$data['username'];
        $pwd=$data['pwd'];

        if ($username=='')
            return json(['result'=>'error','msg'=>'请输入账号']);
        if ($pwd=='')
            return json(['result'=>'error','msg'=>'请输入密码']);

        $user=db('users')->where(['username'=>$username,'password'=>md5($pwd)])->find();


        if (!empty($user))
        {
            if($user['active']==0)
                return json(['result'=>'error','msg'=>'账号还未激活，请重新前往邮箱激活']);
            else{
                $SafeCode=GetRandStr(20);
                db('users')->where(['username'=>$username,'password'=>md5($pwd)])->update(['SafeCode'=>$SafeCode]);
                Monitor('通过app登录了!',$SafeCode);
                return json(['result'=>'success','status'=>1,'SafeCode'=>$SafeCode,'msg'=>'登录成功']);
            }
        }else{
            $confirm=\db('users')->where('username',$username)->find();
            if (!empty($confirm))       //如果存在账号，但是密码不对
                return json(['result'=>'error','msg'=>'存在账号，密码错误']);
            else{       //不存在账号就顺便注册
                $qq=input('param.qq');
                if ($qq=='')
                    return json(['result'=>'error','msg'=>'请先输入qq']);
                $SQL=\db('users')->insert(['username'=>$username,'password'=>md5($pwd),'qq'=>$qq]);
                if ($SQL)
                {
                    $ActiveCode=GetRandStr(20);
                    Db::name('users')->where('qq',$qq)->update(['ActiveCode'=>$ActiveCode]);
                    $this->SendActiveMail($username,$qq,$ActiveCode);
                    return json(['result'=>'success','msg'=>'注册成功,请前往邮箱绑定账号后重新进入app，在绑定成功前，您的账号是无效的']);
                }
            }
        }


    }
    public function GetUserData()   //获取用户信息
    {
        $SafeCode=input('post.SafeCode');
        $UserData=db('users')->where('SafeCode',$SafeCode)->find();

        if(empty($UserData)){
            return json(['result'=>'error']);
        }

        return json($UserData);
    }
}
