<?php
namespace app\home\controller;

use think\Controller;

class History extends Controller
{
    public function voluntary() //志愿者列表
    {
        return $this->fetch('history/voluntary');
    }
    public function VolunJson() //志愿者信息接口
    {
        if(input('post.content'))
        {
            $searchContent=input('post.content');
            $Voluns=db('voluntary')->alias('a')
                ->join('wyz_users b','a.uid=b.Id')
                ->field([   //查询的字段
                    'a.job',
                    'a.name',
                    'a.description',
                    'b.username',
                    'b.sex',
                    'b.qq',
                    'b.phone',
                    'b.college',
                    'b.major',
                    'b.department'
                ])
                ->whereOr('a.job','like','%'.$searchContent.'%')
                ->whereOr('a.name','like','%'.$searchContent.'%')
                ->whereOr('a.description','like','%'.$searchContent.'%')
                ->whereOr('b.username','like','%'.$searchContent.'%')
                ->whereOr('b.qq','like','%'.$searchContent.'%')
                ->whereOr('b.phone','like','%'.$searchContent.'%')
                ->whereOr('b.college','like','%'.$searchContent.'%')
                ->whereOr('b.major','like','%'.$searchContent.'%')
                ->whereOr('b.department','like','%'.$searchContent.'%')
                ->select();
            return json($Voluns);
        }

        $Voluns=db('voluntary')->alias('a')
            ->join('wyz_users b','a.uid=b.Id')
            ->field([   //查询的字段
                'a.job',
                'a.name',
                'a.description',
                'b.username',
                'b.sex',
                'b.qq',
                'b.phone',
                'b.college',
                'b.major',
                'b.department'
            ])
            ->select();
        return json($Voluns);
    }
}
