<?php

namespace app\home\controller;

use app\robot\Zfsoft;
use think\Controller;
use think\response\Json;

class Classroom extends Controller{


    /**
     * 查询页面
     * @return mixed
     */
    public function search(){

       return $this->fetch("classroom/search");

    }


    /**
     * 获取楼号
     * @return Json
     */
    public function BuildingJson(){
        $year=input('post.year');
        $term=input('post.term');

        if(!isset($year)){
            $year=date("Y",time());
        }

        //转换一下学期号
        if($term=='1'){
            $term='3';
        }
        if($term=='2'){
            $term='12';
        }

        //获取网站配置里面的账号密码信息用来实例化
        $Zfsoft=new Zfsoft(C("sid"),C("password"));

        return json($Zfsoft->getBuilding($year,$term));

    }

    public function searchJson(){

        $weeks=input('param.week');
        $days=(int)input('param.day');
        $times=input('param.times');
        $xnm=input('param.xnm');
        $lh=input('param.lh');
        $xqm=input('param.xqm');

        if (!isset($xnm)){
            $xnm=date("Y",time());
        }
        if (!isset($xqm)){
            $xqm='1';
        }

        //缓存标识
        $CacheId=$weeks.$days.$times.$xnm.$lh.$xqm;

        //直接使用缓存
        if(cache($CacheId)){
            return json(cache($CacheId));
        }

        //转换一下学期号
        if($xqm=='1'){
            $xqm='3';
        }
        if($xqm=='2'){
            $xqm='12';
        }

        //在前端，星期号也使用了一样的存储方法，需要处理成星期号数组
        //处理一下星期号，使之变成数组
        $dayArray=[];

        $count=0;
        while($days != 0){

            $count++;

            if($days % 2 ==1){
                array_push($dayArray,(string)$count);
            }
            $days>>=1;
        }

        $ZfSoft=new Zfsoft(C("sid"),C("password"));

        $json=[
            'data'=>$ZfSoft->getRoom($weeks,$dayArray,$times,$xnm,$xqm,$lh)['items'],
            'code'=>200,
        ];

        //缓存起来
        cache($CacheId,$json);

        return json($json);

    }

}