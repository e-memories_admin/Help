<?php
namespace app\home\controller;

use think\Controller;
use think\Db;
use think\Request;

class Search extends Controller
{
    public function myItem()    //我的物品
    {
        return $this->fetch('search/myItem');
    }

    public function RadarListJson() //寻物雷达-失物找回-失物信息接口
    {
        $type=input('get.type');
        if(input('post.'))
        {
            $content=input('post.content');
            $where=[
                ['title','like','%'.$content.'%'],
                ['content','like','%'.$content.'%']
            ];

            if ($type=='master')
                $Lists=db('find_radar')->where(function ($query)use($where){
                    $query->whereOr($where);
                })->where(function ($query){
                        $query->where('master','1');
                })->order('time','asc')->select();
            else
                $Lists=db('find_radar')->where(function ($query)use($where){
                    $query->whereOr($where);
                })->where(function ($query){
                    $query->where('thing','1');
                })->order('time','asc')->select();

        }
        else
        {
            //新增我的列表
            if($type=='my')
            {
                $SafeCode=cookie('SafeCode');
                $uid=\db('users')->where('SafeCode',$SafeCode)->find()['Id'];
                return json(\db('find_radar')->where('uid',$uid)->select());
            }


            if ($type=='master')
                $Lists=db('find_radar')->where('master','1')->order('time','desc')->select();
            else
                $Lists=db('find_radar')->where('thing','1')->order('time','desc')->select();
        }


        //先遍历所有失物表
        foreach ($Lists as $key=>$list)
        {
            //取出失物表的id
            $radar_id=$list['Id'];

            //用失物表id去关系表寻找全部标签
            $tag_lists=db('radar_tag')->alias('a')
                ->join('wyz_tag b','a.tag_id=b.Id')
                ->where('a.radar_id',$radar_id)
                ->field('b.tag_name')
                ->select();

            $tag_name_lists=[];

            foreach ($tag_lists as $tag_list)
            {
                $tag_name_lists[]=$tag_list['tag_name'];
            }

            $Lists[$key]['tags']=$tag_name_lists;
        }

        return json($Lists);
    }

    public function TagList()   //标签名称列表
    {
        return json(db('tag')->select());
    }

    public function SearchByTag()   //标签搜索
    {
        $type=input('get.type');
        switch ($type)  //通过状态值判断操作类型
        {
            case "thing":

                $TagIdList=input('post.content');
                $Lists=db('find_radar')->alias('a')
                    ->group('a.Id')
                    ->field('a.*')
                    ->join('wyz_radar_tag b','a.Id=b.radar_id')
                    ->where('a.thing','1')
                    ->where('b.tag_id','in',$TagIdList)
                    ->order('a.time','asc')
                    ->select();

                break;
            case "master":

                $TagIdList=input('post.content');
                $Lists=db('find_radar')->alias('a')
                    ->group('a.Id')
                    ->field('a.*')
                    ->join('wyz_radar_tag b','a.Id=b.radar_id')
                    ->where('a.master','1')
                    ->where('b.tag_id','in',$TagIdList)
                    ->order('a.time','asc')
                    ->select();

                break;

            case "thingByTagName":

                $TagName=input('post.content');
                $Lists=db('find_radar')->alias('a')
                    ->group('a.Id')
                    ->field('a.*')
                    ->join('wyz_radar_tag b','a.Id=b.radar_id')
                    ->join('wyz_tag c','b.tag_id=c.Id')
                    ->where('a.thing','1')
                    ->where('c.tag_name',$TagName)
                    ->order('a.time','asc')
                    ->select();

                break;

            case "masterByTagName":

                $TagName=input('post.content');
                $Lists=db('find_radar')->alias('a')
                    ->group('a.Id')
                    ->field('a.*')
                    ->join('wyz_radar_tag b','a.Id=b.radar_id')
                    ->join('wyz_tag c','b.tag_id=c.Id')
                    ->where('a.master','1')
                    ->where('c.tag_name',$TagName)
                    ->order('a.time','asc')
                    ->select();
                break;
        }



        /*
         * 通过foreach获取表中的标签信息
         */
        //先遍历所有失物表
        foreach ($Lists as $key=>$list)
        {
            //取出失物表的id
            $radar_id=$list['Id'];

            //用失物表id去关系表寻找全部标签
            $tag_lists=db('radar_tag')->alias('a')
                ->join('wyz_tag b','a.tag_id=b.Id')
                ->where('a.radar_id',$radar_id)
                ->field('b.tag_name')
                ->select();

            $tag_name_lists=[];

            foreach ($tag_lists as $tag_list)
            {
                $tag_name_lists[]=$tag_list['tag_name'];
            }

            $Lists[$key]['tags']=$tag_name_lists;
        }

        Monitor('通过标签搜索失物');

        return json($Lists);

    }

    public function addThing()  //添加寻主信息
    {
        return $this->fetch('search/addThing');
    }

    public function UploadApi() //上传图片接口
    {
        $code=input('post.code');
        $re=new Request();
        $files=$re->file('img_file');
        if($files){

            $info=$files->validate(['ext'=>'png,gif,jpg,jpeg'])->move('upload');
            //改变一下文件限制


            if($info){
                $url='upload/'.$info->getSaveName();
                $sql=\db('find_img')->insert([
                    'url'=>$url,
                    'code'=>$code
                ]);
                if ($sql)
                {
                    Monitor('上传了失物信息');
                    return json(['result'=>'success']);
                }else{
                    return json(['result'=>'error']);
                }

            }else{
                return json(['result'=>'error']);
            }
        }
    }

    public function Release()   //发布接口
    {
        $content=input('post.');

        $insert['title']=$content['title'];
        $insert['content']=$content['content'];
        $insert['hasReward']=$content['hasReward'];
        $insert['reward']=$content['reward'];
        $insert['code']=$content['code'];

        //对发布的信息进行录入
        $SafeCode=cookie('SafeCode');
        if(!$SafeCode)
            return json(['result'=>'error','msg'=>'请先登录']);

        $insert['uid']=\db('users')->where('SafeCode',$SafeCode)->field('Id')->find()['Id'];
        $insert['time']= date('Y-m-d H:i:s',time());


        //判断发布类型
        if(input('get.type')=='thing')
            $insert['master']=1;
        else
            $insert['thing']=1;

        //插入信息并获取主键Id
        $radar_id=\db('find_radar')->insertGetId($insert);

        $tags=explode(",",$content['tag']);


        //对tag进行操作
        $success=0;
        $error=0;
        foreach ($tags as $tag)
        {
            if ($tag=='')
                continue;
            $tagData=\db('tag')->where('tag_name',$tag)->find();

            //如果数据库已经有这个标签了
            if(!empty($tagData))
            {
                //就建立关联
                $sql=\db('radar_tag')->insert([
                    'radar_id'=>$radar_id,
                    'tag_id'=>$tagData['Id']
                ]);
                if ($sql)
                {
                    $success++;
                }
                else
                {
                    $error++;
                }
            }
            else
            {
                //先插入这个标签名称再建立关联
                $tag_id=\db('tag')->insertGetId([
                    'tag_name'=>$tag
                ]);
                $sql=\db('radar_tag')->insert([
                    'radar_id'=>$radar_id,
                    'tag_id'=>$tag_id
                ]);
                if ($sql)
                {
                    $success++;
                }
                else
                {
                    $error++;
                }
            }
        }
        if ($success!=0)
        {
            Monitor('发布了失物信息');
            return json(['result'=>'success']);
        }
        else
            return json(['result'=>'error']);
    }

    public function thingDetail()   //失物详情
    {
        return $this->fetch('search/thingDetail');
    }

    public function thingDetailJson()   //失物详情接口
    {
        $radar_id=input('get.radar_id');
        $Radar=\db('find_radar')->where('Id',$radar_id)->find();

        $code=$Radar['code'];

        $Images=\db('find_img')->where('code',$code)->select();

        $Radar['img']=$Images;

        return json($Radar);
    }

    public function ConfirmJson()   //确认接口
    {
        $SafeCode=cookie('SafeCode');

        $to_id=input('post.to_id');
        $fromData=\db('users')->where('SafeCode',$SafeCode)->find();
        $toData=\db('users')->where('Id',$to_id)->find();

        $body="对方的QQ号为:".$fromData['qq'].",请尽快联系，确认后请回到平台更新信息";
        send_mail($toData['qq'].'@qq.com',$fromData['name'],'您的寻物雷达有消息了！',$body);
        Monitor('获得了失物/失主的信息');
        return json(['result'=>'success','msg'=>'对方QQ为：'.$toData['qq'].'请尽快联系']);
    }

    public function Confirm()   //确认完成接口，与上面不一样
    {
        $SafeCode=cookie('SafeCode');
        $uid=\db('users')->where('SafeCode',$SafeCode)->find()['Id'];
        $radar_id=input('post.radar_id');

        $sql=\db('find_radar')->where('Id',$radar_id)->where('uid',$uid)->update(['complete'=>1]);
        if ($sql)
        {
            Monitor('找到了失物/失主');
            return json(['result'=>'success']);
        }
        else
            return json(['result'=>'error']);
    }

}