<?php
/*权限:
1:校青协主席
2:会长
3:管理员
*/
namespace app\home\controller;
use think\Controller;
use think\Db;

class Bbs extends Controller{
    public function index() //留言板主页
    {
        return $this->fetch("bbs/index");
    }
    public  function PageNum()  //留言的页数
    {
        $EachNum=6; //分页每页的记录个数
        $BbsNum=Db::name('bbs')->count();
        return json(["num"=>ceil($BbsNum/$EachNum)]);
    }
    public function BbsJson()   //留言信息接口
    {
        $EachNum=6; //每页数量
        $Page=input('post.page');
        if($Page==null)
        {
            $Page=1;
        }
        $FromData=((int)$Page-1)*$EachNum;
        $PageData=Db::name('bbs')->order("time","desc")->limit($FromData,$EachNum)->select();
        return json($PageData);
    }
    public function CommentJson()    //评论获取接口
    {
        $Upper=input('post.BbsId');
        $CommentData=Db::name('comment')->where("Upper",$Upper)->order("time","desc")->select();
        return json($CommentData);
    }
    public function DeleteBbs() //删除留言信息的接口
    {
        $SafeCode=cookie("SafeCode");
        $PersonData=Db::name('users')->where('SafeCode',$SafeCode)->find();
        $Uid=$PersonData["Id"];

        $BbsId=input('post.Id');
        $BbsData=Db::name("bbs")->where("Id",$BbsId)->find();
        if($BbsData["uid"]!=$Uid&&$PersonData["auth"]!=3)   //管理员有删除权限
        {
            return json(["result"=>"error","msg"=>"不是你的留言，无权操作"]);
        }

        $SQL=Db::name("bbs")->where("Id",$BbsId)->delete();
        if($SQL)
        {
            Monitor('删除了留言');
            return json(["result"=>"success"]);
        }else{
            return json(["result"=>"error","msg"=>"未知错误"]);
        }
    }
    public function DeleteComment() //删除评论信息的接口
    {
        $SafeCode=cookie("SafeCode");
        $PersonData=Db::name('users')->where('SafeCode',$SafeCode)->find();
        $Uid=$PersonData["Id"];

        $CommentId=input('post.Id');
        $CommentData=Db::name("comment")->where("Id",$CommentId)->find();
        if($CommentData["uid"]!=$Uid&&$PersonData["auth"]!=3)   //管理员有删除权限
        {
            return json(["result"=>"error","msg"=>"不是你的留言，无权操作"]);
        }

        $SQL=Db::name("comment")->where("Id",$CommentId)->delete();
        if($SQL)
        {
            Monitor('删除了评论');
            return json(["result"=>"success"]);
        }else{
            return json(["result"=>"error","msg"=>"未知错误"]);
        }
    }
    public function SendComment()    //评论接口
    {
        $Upper=input('post.Upper');
        $content=input('post.content');

        //获取用户信息
        $SafeCode=cookie("SafeCode");
        $PersonData=Db::name('users')->where('SafeCode',$SafeCode)->find();

        $qq=$PersonData["qq"];
        $name=$PersonData["name"];
        $uid=$PersonData["Id"];
        $time=date("Y-m-d H:i:s",time());

        $SQL=Db::name('comment')->insert([
            "name"=>$name,
            "qq"=>$qq,
            "content"=>$content,
            "time"=>$time,
            "Upper"=>$Upper,
            "uid"=>$uid
        ]);
        if($SQL)
        {
            Monitor('发表了评论');
            return json(["result"=>"success"]);
        }else{
            return json(["result"=>"error","msg"=>"未知错误"]);
        }
    }
    public function AddBbs()    //留言页面
    {
        return $this->fetch("bbs/AddBbs");
    }
    public function AddBbsJson()    //提交留言接口
    {
        $content=input('post.content');
        //获取用户信息
        $SafeCode=cookie("SafeCode");
        $PersonData=Db::name('users')->where('SafeCode',$SafeCode)->find();

        $qq=$PersonData["qq"];
        $name=$PersonData["name"];
        $uid=$PersonData["Id"];
        $time=date("Y-m-d H:i:s",time());

        $SQL=Db::name('bbs')->insert([
            "name"=>$name,
            "qq"=>$qq,
            "content"=>$content,
            "time"=>$time,
            "uid"=>$uid
        ]);
        if($SQL)
        {
            Monitor('发表了留言');
            return json(["result"=>"success"]);
        }else{
            return json(["result"=>"error","msg"=>"未知错误"]);
        }
    }
}