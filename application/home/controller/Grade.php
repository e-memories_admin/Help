<?php
namespace app\home\controller;

use think\Controller;

class Grade extends Controller
{
    public function insert_sid()    //更改教务系统账号信息
    {
        return $this->fetch('grade/insert_sid');
    }

    public function currentSid()    //当前教务系统账号密码
    {
        $SafeCode=cookie('SafeCode');
        $uid=db('users')->where('SafeCode',$SafeCode)->find()['Id'];

        $Sid=db('sid')->where('uid',$uid)->find();
        if (!empty($Sid))
            return json($Sid);
        else
            return json([]);
    }

    /**
     *
     * 更新教务系统账号密码
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function updateSid()
    {
        $content=input('post.');
        unset($content['Id']);

        //获取当前用户教务系统信息
        $SafeCode=\cookie('SafeCode');
        $uid=\db('users')->where('SafeCode',$SafeCode)->value('Id');
        $Sid=\db('sid')->where('uid',$uid)->find();

        //如果存在信息就更新，不存在就插入
        if (!empty($Sid)){
            $Sql=\db('sid')->where('uid',$uid)->update($content);
        }else{

            $content['uid']=$uid;
            $Sql=\db('sid')->insert($content);
        }

        if ($Sql)
        {
            Monitor('更新了自己的教务系统信息');
            return json(['result'=>'success']);
        }else{
            return json(['result'=>'error']);
        }
    }

    /**
     * 获取成绩接口
     */
    public function score()
    {    //获取成绩
        return $this->fetch('grade/score');
    }

    /**
     * 成绩接口，对接bootstrap-table
     */
    public function score_json()
    {


        //根据Cookie获取用户教务系统信息
        if(\cookie('SafeCode'))
        {
            $SafeCode = \cookie('SafeCode');
            $user = \db('users')->where('SafeCode',$SafeCode)->find();
        }else{
            $username=input('get.username');
            $pwd=input('get.pwd');
            $user=\db('user')->where(['username'=>$username,'password'=>md5($pwd)])->find();
        }
        $uid = $user['Id'];
        $Sid=\db('sid')->where('uid',$uid)->find();


        //获取输入的年份和学期以确定输出的成绩数据是否为某学期的还是全部
        if (input('get.year') && input('get.term')) {

            //获取输入的学年学期号
            $year=input('get.year');
            $term=input('get.term');

            //成绩访问接口
//            $Api=new Robot($uid,$Sid['sid'],$Sid['pwd']);
//            $Api->getScore($year,$term);

            //从数据库中获取该学期成绩
            $scores = \db('grade')->where(['uid' => $uid, 'year' => $year, 'term' => $term])->select();

        } else {

            //从数据库中获取该用户全部成绩
            $scores = \db('grade')->where(['uid' => $uid])->where('is_sum', 0)->select();

        }

        //统计不同科目考试最大总人数
        $sum_num=0;

        //使用遍历来更新成绩信息
        foreach ($scores as $score) {

            //找出所有比自己成绩高的同学的成绩的个数以确定自己的名次（每一科）
            $num = \db('grade')->where( 'courseCode',$score['courseCode'])
                ->where('uid', '<>', $score['uid'])
                ->where('score', '>', $score['score'])
                ->where(['year' => $score['year'], 'term' => $score['term']])
                ->count();

            //当前科目的总人数
            $nums = \db('grade')->where('courseCode',$score['courseCode'])
                ->where(['year' => $score['year'], 'term' => $score['term']])
                ->count();

            $sum_num=max([$nums,$sum_num]);

            //更新每一科的名次
            \db('grade')->where('uid', $score['uid'])->where('courseCode', $score['courseCode'])->update(['order' => $num + 1]);
        }

        //如果输入了准确的学年学期信息就查询学年学期的成绩，否则就查出全部成绩
        if (input('get.year') && input('get.term')) {
            $year=input('get.year');
            $term=input('get.term');
            $scores = \db('grade')->where(['uid' => $uid, 'year' => $year, 'term' => $term])->select();
        } else {
            $scores = \db('grade')->where(['uid' => $uid])->where('is_sum', 0)->select();
        }

        $arr = [
            'code' => 200,     //状态码为200
            'data' => $scores, //成绩数据
            'sum' => $sum_num
        ];
        Monitor('查询更新了自己的成绩');

        return json($arr);
    }
}