<?php
namespace app\home\controller;

use think\Controller;
use think\Request;

class Pet extends Controller
{
    public function adobt() //领养页面
    {
        return $this->fetch('pet/adobt');
    }

    public function PetListJson()   //宠物列表接口
    {
        if (input('post.'))
        {
            $content=input('post.content');
            $where=[
                ['name','like','%'.$content.'%'],
                ['varieties','like','%'.$content.'%'],
                ['age','like','%'.$content.'%'],
                ['description','like','%'.$content.'%'],
            ];
            $PetList=db('pet')->whereOr($where)->order('time','asc')->select();
        }
        else
        {
            $PetList=db('pet')->order('time','asc')->select();
        }

        return json($PetList);
    }

    public function PetDetail() //宠物详情
    {
        return $this->fetch('pet/petDetail');
    }

    public function PetDetailJson() //宠物详情接口
    {
        $pet_id=input('get.pet_id');
        $Pet=db('pet')->alias('a')
            ->join('wyz_users b','b.Id=a.uid')
            ->field('b.name as Name,a.*,b.qq')
            ->where('a.Id',$pet_id)->find();

        $Pet['img']=db('find_img')->where('code',$Pet['code'])->select();
        return json($Pet);
    }

    public function Confirm()   //确认被领养
    {
        $SafeCode=cookie('SafeCode');
        $uid=\db('users')->where('SafeCode',$SafeCode)->find()['Id'];
        $pet_id=input('post.pet_id');

        $sql=\db('pet')->where('Id',$pet_id)->where('uid',$uid)->update(['adobted'=>1]);
        if ($sql)
        {
            Monitor('更新了自己发布的宠物状态：已领养');
            return json(['result'=>'success']);
        }
        else
            return json(['result'=>'error']);

    }

    public function ConfirmJson()   //领养接口
    {
        $SafeCode=cookie('SafeCode');

        $to_id=input('post.to_id');
        $fromData=\db('users')->where('SafeCode',$SafeCode)->find();
        $toData=\db('users')->where('Id',$to_id)->find();

        $body="对方的QQ号为:".$fromData['qq'].",确认对方领养宠物后，请回到平台更新信息";
        send_mail($toData['qq'].'@qq.com',$fromData['name'],'有人想要领养你的宠物！',$body);
        Monitor('申请了宠物的领养');
        return json(['result'=>'success','msg'=>'请联系QQ：'.$toData['qq'].'获取该宠物的更多信息']);
    }

    public function UploadApi() //上传图片接口
    {
        $code=input('post.code');
        $re=new Request();
        $files=$re->file('img_file');
        if($files){

            $info=$files->validate(['ext'=>'png,gif,jpg,jpeg'])->move('upload');
            //改变一下文件限制


            if($info){
                $url='upload/'.$info->getSaveName();
                $sql=\db('find_img')->insert([
                    'url'=>$url,
                    'code'=>$code
                ]);
                if ($sql)
                {
                    Monitor('上传了图片');
                    return json(['result'=>'success']);
                }

            }else{
                return json(['result'=>'error']);
            }
        }
    }

    public function Release()   //发布接口
    {
        $content=input('post.');
        //对发布的信息进行录入
        $SafeCode=cookie('SafeCode');
        if(!$SafeCode)
            return json(['result'=>'error','msg'=>'请先登录']);

        $content['uid']=\db('users')->where('SafeCode',$SafeCode)->field('Id')->find()['Id'];
        $content['time']= date('Y-m-d H:i:s',time());

        $sql=db('pet')->insert($content);


        if ($sql)
        {
            Monitor('发布了宠物信息');
            return json(['result'=>'success']);
        }
        else
            return json(['result'=>'error']);
    }

    public function addPet()    //新增宠物信息
    {
        return $this->fetch('pet/addPet');
    }
}
