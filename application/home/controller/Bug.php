<?php
namespace app\home\controller;

use think\Controller;
use think\Request;

class Bug extends Controller
{
    public function addBug() //bug提交页面
    {
        return $this->fetch('bug/addBug');
    }

    public function UploadApi() //上传图片接口
    {
        $code=input('post.code');
        $re=new Request();
        $files=$re->file('img_file');
        if($files){

            $info=$files->validate(['ext'=>'png,gif,jpg,jpeg'])->move('upload');
            //TODO （如有需要）改变一下文件限制

            if($info){
                $url='upload/'.$info->getSaveName();
                $sql=\db('find_img')->insert([
                    'url'=>$url,
                    'code'=>$code
                ]);
                if ($sql)
                {
                    Monitor('上传了bug的图片');
                    return json(['result'=>'success']);
                }


            }else{
                return json(['result'=>'error']);
            }
        }
    }

    public function addBugJson()
    {
        $content=input('post.');
        $SafeCode=cookie('SafeCode');
        $content['uid']=db('users')->where('SafeCode',$SafeCode)->find()['Id'];
        $content['time']=date('Y-m-d H:i:s',time());

        //发送bug给开发者
        $body="”".$content['function']."“功能出现了新的bug，bug的描述为：<br>".$content['description']."<br>提交时间：".$content['time'];

        $img=db('find_img')->where('code',$content['code'])->select();
        foreach ($img as $i)
        {
            $body=$body."<img src='http://".$_SERVER['HTTP_HOST']."/".$i['url']."'>";
        }
        send_mail(C('auth_mail'),'志愿服务平台','工程师，出现了新的bug啦！',$body);

        $Sql=db('bug')->insert($content);
        if ($Sql)
        {
            Monitor('提交了bug');
            return json(['result'=>'success']);
        }

        else
            return json(['result'=>'error']);

    }

    public function BugJson()   //bug返回接口
    {
        $BugData=db('bug')->alias('a')
            ->join('wyz_users b','a.uid=b.Id')
            ->order('time','asc')
            ->select();

        foreach ($BugData as $key=>$bug)
        {
            $code=$bug['code'];
            $BugData[$key]['img']=db('find_img')->where('code',$code)->select();
        }

        return json($BugData);
    }
}
