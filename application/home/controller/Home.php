<?php
/*权限:
1:校青协主席
2:会长
3:管理员
4:部长
5：干事
0：普通学生
*/
namespace app\home\controller;

use think\Controller;
use think\Db;
use think\Exception;

class Home extends Controller{
    public function index() //主页部分
    {
        $SafeCode=cookie('SafeCode');
        $UserData=Db::name('users')->field('password',true)->where('SafeCode',$SafeCode)->find();

        //删除Cookie后重定向
        if (empty($UserData)){
            cookie('SafeCode',null);
            $this->redirect("");
        }

        return $this->fetch('index/index');
    }

    public function home()  //首页部分
    {
        return $this->fetch('index/home');
    }

    public function UserDataJson()  //用户信息接口
    {
        $NumToAuth=["学生","校青协主席","会长","网站管理员","部长","干事"];
        $SafeCode=cookie('SafeCode');
        $UserData=Db::name('users')->field('password',true)->where('SafeCode',$SafeCode)->find();

        if(in_array($UserData['auth'],[2,3,4])) $UserData['operator']=true;
        else $UserData['operator']=false;

        $UserData["auth"]=$NumToAuth[$UserData["auth"]];
        return json($UserData);
    }

    public function MonitorJson()   //操作接口
    {
        $SafeCode=\cookie('SafeCode');
        $uid=\db('users')->where('SafeCode',$SafeCode)->find()['Id'];
        $MonitorData=\db('monitor')->where('uid',$uid)->limit(5)->order('time','desc')->select();

        return json($MonitorData);
    }

    public function DepartJson() //部门信息接口
    {
        $DepartData=Db::name('depart')->select();
        return json($DepartData);
    }

    public function CollegeJson() //学院信息接口
    {
        $CollegeData=Db::name('college')->select();
        return json([
            'message'=>"",
            'value'=>$CollegeData,
            'code'=>'200',
            'redirect'=>''
        ]);
    }

    public function MajorJson() //专业信息接口
    {
        $MajorData=Db::name('major')->select();

        return json([
            'message'=>"",
            'value'=>$MajorData,
            'code'=>'200',
            'redirect'=>''
        ]);
    }

    public function EditPerson()    //用户信息修改
    {
        return $this->fetch("home/EditPerson");
    }

    public function EditJson()  //信息修改接口
    {
        //获取编辑的信息
        $Edit=input('post.');

        //获取当前用户信息
        $SafeCode=\cookie("SafeCode");
        $UserData=Db::name("users")->where("SafeCode",$SafeCode)->find();
        $Id=$UserData["Id"];

        //查询学院是否存在
        //不存在就插入学院数据
        $CollegeData=Db::name('college')->where("college_name",$Edit["college"])->find();
        if(empty($CollegeData))
        {
            Db::name('college')->insert(["college_name"=>$Edit["college"]]);
        }

        //查询专业是否存在
        //不存在就插入专业数据
        $MajorData=Db::name('major')->where("major_name",$Edit["major"])->find();
        if(empty($MajorData))
        {
            Db::name('major')->insert(["major_name"=>$Edit["major"]]);
        }

        //防止黑客传入其他数据越权操作
        $Update["name"]=$Edit["name"];
        $Update["sex"]=$Edit["sex"];
        $Update["phone"]=$Edit["phone"];
        $Update["major"]=$Edit["major"];
        $Update["college"]=$Edit["college"];
        $Update["sid"]=$Edit["sid"];
        $Update["identity_card"]=$Edit["identity_card"];

        //更新信息
        try {
            \db('users')->where('Id',$Id)->update($Update);
            Monitor('修改了自己的个人信息');
            return json(["result"=>"success"]);
        }catch (Exception $e){
            return json(["result"=>"error","msg"=>"未知错误"]);
        }

    }

    public function MsgJson()   //未读消息接口
    {
        //获取Uid
        $SafeCode=\cookie("SafeCode");
        $UserData=Db::name('users')->where("SafeCode",$SafeCode)->find();
        $Uid=$UserData["Id"];

        $Unread=Db::name('msg_read')->field('message_id')->where(["uid"=>$Uid,"read"=>false])->order('time')->limit(5)->select();
        $Count=count($Unread);
        $MagData=[];
        foreach ($Unread as $key=>$datum)   //获取未读消息
        {
            $Data=Db::name('message')->where("Id",$datum["message_id"])->find();
            $Data["content"]=htmlspecialchars(trim(strip_tags($Data["content"])));
            $Data["content"]=substr($Data["content"],0,40);
            $Data["content"]=mb_convert_encoding( $Data["content"], 'UTF-8', 'UTF-8,GBK,GB2312,BIG5' );
            $MagData[]=$Data;
        }
        return json(["num"=>$Count,"data"=>$MagData]);
    }

}
